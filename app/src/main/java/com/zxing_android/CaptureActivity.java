package com.zxing_android;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.activity.BaseActivity;
import com.xht.logisticssystem.activity.EmptyInfoActivity;
import com.xht.logisticssystem.activity.OrderDetailActivity;
import com.xht.logisticssystem.bean.LogisticsBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.zxing_android.camera.CameraManager;
import com.zxing_android.decoding.CaptureActivityHandler;
import com.zxing_android.decoding.InactivityTimer;
import com.zxing_android.decoding.RGBLuminanceSource;
import com.zxing_android.decoding.Utils;
import com.zxing_android.view.ViewfinderView;


import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.Vector;

/**
 * 拍照的Activity
 * 
 * @author Baozi
 * 
 */
public class CaptureActivity extends BaseActivity implements Callback {

	private static final int REQUEST_CODE = 234;
	private CaptureActivityHandler handler;
	private ViewfinderView viewfinderView;
	private boolean hasSurface;
	private Vector<BarcodeFormat> decodeFormats;
	private String characterSet;
	private InactivityTimer inactivityTimer;
	private MediaPlayer mediaPlayer;
	private boolean playBeep;
	private static final float BEEP_VOLUME = 0.10f;
	private boolean vibrate;
	private String photo_path;
	private Bitmap scanBitmap;
	private final int RESULT_CODE_STARTSTORAGE=100;

	private ImageView iv_open_light, iv_back;
	private TextView tv_light;
	private LinearLayout ll_light, ll_enter_code, ll_enter_code_view;
	private EditText et_enter_code;
	private RelativeLayout rl_switch_scan_code;
	private TextView tv_confirm,tv_scan_tip;

	private boolean isLightOn = false;
	private boolean isEnterCode = false;//是否是手动输入模式


//	/** Called when the activity is first created. */
//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		setContentView(R.layout.mo_scanner_main);
//		// 初始化 CameraManager
//		CameraManager.init(getApplication());
//		viewfinderView = (ViewfinderView) findViewById(R.id.mo_scanner_viewfinder_view);
//		initView();
//
//		hasSurface = false;
//		inactivityTimer = new InactivityTimer(this);
//
//	}


	@Override
	public int setLayoutResId() {
		return  R.layout.activity_capture;
	}

	@Override
	public void initView() {
		// 初始化 CameraManager
		findViewById();
		hasSurface = false;
		CameraManager.init(getApplication());
		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
		hasSurface = false;
		inactivityTimer = new InactivityTimer(this);

		iv_open_light.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isLightOn) {
					isLightOn = false;
					CameraManager.get().offLight();
					tv_light.setText(R.string.open_light);
					iv_open_light.setImageResource(R.mipmap.scan_openflashlight);
				} else {
					isLightOn = true;
					CameraManager.get().openLight();
					tv_light.setText(R.string.close_light);
					iv_open_light.setImageResource(R.mipmap.scan_closeflashlight);
				}
			}
		});
		ll_enter_code.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ll_enter_code_view.setVisibility(View.VISIBLE);
				viewfinderView.setVisibility(View.GONE);
				ll_enter_code.setVisibility(View.GONE);
				ll_light.setVisibility(View.GONE);
				tv_scan_tip.setVisibility(View.GONE);
				isEnterCode = true;
				if(isLightOn){
					isLightOn = false;
					CameraManager.get().offLight();
				}
			}
		});
		rl_switch_scan_code.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ll_enter_code_view.setVisibility(View.GONE);
				viewfinderView.setVisibility(View.VISIBLE);
				ll_enter_code.setVisibility(View.VISIBLE);
				ll_light.setVisibility(View.VISIBLE);
				tv_scan_tip.setVisibility(View.VISIBLE);
				isEnterCode = false;
			}
		});
		iv_back.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});

		tv_confirm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!TextUtils.isEmpty(et_enter_code.getText().toString().trim())) {
					requestLogistics(et_enter_code.getText().toString().trim());
				}
			}
		});
	}

	private void findViewById() {
		iv_open_light = findViewById(R.id.iv_open_light);
		tv_light = findViewById(R.id.tv_light);
		iv_back = findViewById(R.id.iv_back);
		ll_light = findViewById(R.id.ll_light);
		ll_enter_code = findViewById(R.id.ll_enter_code);
		ll_enter_code_view = findViewById(R.id.ll_enter_code_view);
		et_enter_code = findViewById(R.id.et_enter_code);
		rl_switch_scan_code = findViewById(R.id.rl_switch_scan_code);
		tv_confirm = findViewById(R.id.tv_confirm);
		tv_scan_tip=findViewById(R.id.tv_scan_tip);
	}

	private void requestLogistics(String code) {
		NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGISTICS_INFO())
				.addParam("sn_or_shipping", code)
				.withPOST(new NetCallBack<LogisticsBean>(this) {
					@Override
					public void onSuccess(@NonNull LogisticsBean logisticsBean) {
						Intent intent = new Intent(CaptureActivity.this, OrderDetailActivity.class);
						intent.putExtra(OrderDetailActivity.KEY_ORDERDETAIL, logisticsBean);
						startActivity(intent);
						finish();
					}

					@Override
					public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
						if (errCode == 30001) {
							//无此订单物流
							Intent intent = new Intent(CaptureActivity.this, EmptyInfoActivity.class);
							startActivity(intent);
							finish();
						} else {
							DialogUtils.createOneBtnDialog(CaptureActivity.this, err, getString(R.string.confirm), new DialogUtils.OnLeftBtnListener() {
								@Override
								public void setOnLeftListener(Dialog dialog) {
									if (isEnterCode) {
										et_enter_code.setText("");
									} else {
										restartPreviewAfterDelay(0L);
									}
								}
							}, false, false);
						}
					}

					@NotNull
					@Override
					public Class<LogisticsBean> getRealType() {
						return LogisticsBean.class;
					}
				}, false);
	}


	boolean flag = false;

	protected void light() {
		if (flag == true) {
			flag = false;
			// 关闪光灯
			CameraManager.get().offLight();

		} else {
			flag = true;
			// 开闪光灯
			CameraManager.get().openLight();

		}

	}

	private void photo() {

		Intent innerIntent = new Intent(); // "android.intent.action.GET_CONTENT"
		if (Build.VERSION.SDK_INT < 19) {
			innerIntent.setAction(Intent.ACTION_GET_CONTENT);
		} else {
			innerIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
		}
		// innerIntent.setAction(Intent.ACTION_GET_CONTENT);

		innerIntent.setType("image/*");

		Intent wrapperIntent = Intent.createChooser(innerIntent, "选择二维码图片");

		CaptureActivity.this
				.startActivityForResult(wrapperIntent, REQUEST_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {

			switch (requestCode) {

			case REQUEST_CODE:

				String[] proj = { MediaStore.Images.Media.DATA };
				// 获取选中图片的路径
				Cursor cursor = getContentResolver().query(data.getData(),
						proj, null, null, null);

				if (cursor.moveToFirst()) {

					int column_index = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					photo_path = cursor.getString(column_index);
					if (photo_path == null) {
						photo_path = Utils.getPath(getApplicationContext(),
								data.getData());
						Log.i("123path  Utils", photo_path);
					}
					Log.i("123path", photo_path);

				}

				cursor.close();

				new Thread(new Runnable() {

					@Override
					public void run() {

						Result result = scanningImage(photo_path);
						// String result = decode(photo_path);
						if (result == null) {
							Log.i("123", "   -----------");
							Looper.prepare();
							Toast.makeText(getApplicationContext(), "图片格式有误", Toast.LENGTH_SHORT)
									.show();
							Looper.loop();
						} else {
							Log.i("123result", result.toString());
							// Log.i("123result", result.getText());
							// 数据返回
							String recode = recode(result.toString());
							Intent data = new Intent();
							data.putExtra("result", recode);
							setResult(300, data);
							finish();
						}
					}
				}).start();
				break;

			}

		}

	}

	// TODO: 解析部分图片
	protected Result scanningImage(String path) {
		if (TextUtils.isEmpty(path)) {

			return null;

		}
		// DecodeHintType 和EncodeHintType
		Hashtable<DecodeHintType, String> hints = new Hashtable<DecodeHintType, String>();
		hints.put(DecodeHintType.CHARACTER_SET, "utf-8"); // 设置二维码内容的编码
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true; // 先获取原大小
		scanBitmap = BitmapFactory.decodeFile(path, options);
		options.inJustDecodeBounds = false; // 获取新的大小

		int sampleSize = (int) (options.outHeight / (float) 200);

		if (sampleSize <= 0)
			sampleSize = 1;
		options.inSampleSize = sampleSize;
		scanBitmap = BitmapFactory.decodeFile(path, options);

		// --------------测试的解析方法---PlanarYUVLuminanceSource-这几行代码对project没作功----------

		LuminanceSource source1 = new PlanarYUVLuminanceSource(
				rgb2YUV(scanBitmap), scanBitmap.getWidth(),
				scanBitmap.getHeight(), 0, 0, scanBitmap.getWidth(),
				scanBitmap.getHeight(), false);
		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
				source1));
		MultiFormatReader reader1 = new MultiFormatReader();
		Result result1;
		try {
			result1 = reader1.decode(binaryBitmap);
			String content = result1.getText();
			Log.i("123content", content);
		} catch (NotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// ----------------------------

		RGBLuminanceSource source = new RGBLuminanceSource(scanBitmap);
		BinaryBitmap bitmap1 = new BinaryBitmap(new HybridBinarizer(source));
		QRCodeReader reader = new QRCodeReader();
		try {

			return reader.decode(bitmap1, hints);

		} catch (NotFoundException e) {

			e.printStackTrace();

		} catch (ChecksumException e) {

			e.printStackTrace();

		} catch (FormatException e) {

			e.printStackTrace();

		}

		return null;

	}

	@Override
	protected void onResume() {
		super.onResume();
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			initCamera(surfaceHolder);
		} else {
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		decodeFormats = null;
		characterSet = null;

		playBeep = true;
		AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
		if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
			playBeep = false;
		}
		initBeepSound();
		vibrate = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		CameraManager.get().closeDriver();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		super.onDestroy();

	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		try {
			CameraManager.get().openDriver(surfaceHolder);
		} catch (IOException ioe) {
			return;
		} catch (RuntimeException e) {
			return;
		}
		if (handler == null) {
			handler = new CaptureActivityHandler(CaptureActivity.this, decodeFormats,
					characterSet);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;

	}

	public ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	public void drawViewfinder() {
		// viewfinderView.drawViewfinder();

	}

	public void handleDecode(final Result result, Bitmap barcode) {
		inactivityTimer.onActivity();
		playBeepSoundAndVibrate();
		String recode = recode(result.toString());
		requestLogistics(recode);
	}

	private void initBeepSound() {
		if (playBeep && mediaPlayer == null) {
			// The volume on STREAM_SYSTEM is not adjustable, and users found it
			// too loud,
			// so we now play on the music stream.
			setVolumeControlStream(AudioManager.STREAM_MUSIC);
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setOnCompletionListener(beepListener);

			AssetFileDescriptor file = getResources().openRawResourceFd(
					R.raw.beep);
			try {
				mediaPlayer.setDataSource(file.getFileDescriptor(),
						file.getStartOffset(), file.getLength());
				file.close();
				mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
				mediaPlayer.prepare();
			} catch (IOException e) {
				mediaPlayer = null;
			}
		}
	}

	private static final long VIBRATE_DURATION = 200L;

	private void playBeepSoundAndVibrate() {
		if (playBeep && mediaPlayer != null) {
			mediaPlayer.start();
		}
		if (vibrate) {
			Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATE_DURATION);
		}
	}

	/**
	 * When the beep has finished playing, rewind to queue up another one.
	 */
	private final OnCompletionListener beepListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mediaPlayer) {
			mediaPlayer.seekTo(0);
		}
	};

	/**
	 * 中文乱码
	 * 
	 * 暂时解决大部分的中文乱码 但是还有部分的乱码无法解决 .
	 * 
	 * 如果您有好的解决方式 请联系 2221673069@qq.com
	 * 
	 * 我会很乐意向您请教 谢谢您
	 * 
	 * @return
	 */
	private String recode(String str) {
		String formart = "";

		try {
			boolean ISO = Charset.forName("ISO-8859-1").newEncoder()
					.canEncode(str);
			if (ISO) {
				formart = new String(str.getBytes("ISO-8859-1"), "GB2312");
				Log.i("1234      ISO8859-1", formart);
			} else {
				formart = str;
				Log.i("1234      stringExtra", str);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formart;
	}

	/**
	 * //TODO: TAOTAO 将bitmap由RGB转换为YUV //TOOD: 研究中
	 * 
	 * @param bitmap
	 *            转换的图形
	 * @return YUV数据
	 */
	public byte[] rgb2YUV(Bitmap bitmap) {
		// 该方法来自QQ空间
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		int[] pixels = new int[width * height];
		bitmap.getPixels(pixels, 0, width, 0, 0, width, height);

		int len = width * height;
		byte[] yuv = new byte[len * 3 / 2];
		int y, u, v;
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				int rgb = pixels[i * width + j] & 0x00FFFFFF;

				int r = rgb & 0xFF;
				int g = (rgb >> 8) & 0xFF;
				int b = (rgb >> 16) & 0xFF;

				y = ((66 * r + 129 * g + 25 * b + 128) >> 8) + 16;
				u = ((-38 * r - 74 * g + 112 * b + 128) >> 8) + 128;
				v = ((112 * r - 94 * g - 18 * b + 128) >> 8) + 128;

				y = y < 16 ? 16 : (y > 255 ? 255 : y);
				u = u < 0 ? 0 : (u > 255 ? 255 : u);
				v = v < 0 ? 0 : (v > 255 ? 255 : v);

				yuv[i * width + j] = (byte) y;
				// yuv[len + (i >> 1) * width + (j & ~1) + 0] = (byte) u;
				// yuv[len + (i >> 1) * width + (j & ~1) + 1] = (byte) v;
			}
		}
		return yuv;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch(requestCode){
			case RESULT_CODE_STARTSTORAGE:
				boolean albumAccepted = grantResults[0]==PackageManager.PERMISSION_GRANTED;
				if(albumAccepted){
					Intent intent = new Intent();
					intent.setAction(Intent.ACTION_PICK);
					intent.setType("image/*");
					startActivityForResult(intent, REQUEST_CODE);
				}else{
					//用户授权拒绝之后，友情提示一下就可以了
					Toast.makeText(CaptureActivity.this,"请开启应用相册权限",Toast.LENGTH_SHORT).show();
				}
				break;
		}
	}

	/**
	 * 重新扫描
	 *
	 * @param delayMS
	 */
	public void restartPreviewAfterDelay(long delayMS) {
		if (handler != null) {
			handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
		}
//        resetStatusView();
	}

}