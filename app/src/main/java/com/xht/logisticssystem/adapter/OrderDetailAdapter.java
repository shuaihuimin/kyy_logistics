package com.xht.logisticssystem.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.logisticssystem.R;
import com.xht.logisticssystem.activity.GoodsListActivity;
import com.xht.logisticssystem.activity.OrderDetailActivity;
import com.xht.logisticssystem.bean.LogisticsBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Utils;

import org.jetbrains.annotations.NotNull;

public class OrderDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private LogisticsBean mLogisticsBean;

    private final int ITEM_STORE = 0;
    private final int ITEM_STORE_LOGISTICS = 1;
    private OnRefreshOrderDetail mListener;

    public OrderDetailAdapter(Context context, LogisticsBean logisticsBean,OnRefreshOrderDetail listener) {
        this.mContext = context;
        this.mLogisticsBean = logisticsBean;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == ITEM_STORE) {
            View itemView = View.inflate(mContext, R.layout.item_store, null);
            return new StoreViewHolder(itemView);
        }
        if (viewType == ITEM_STORE_LOGISTICS) {
            View itemView = View.inflate(mContext, R.layout.item_store_logistics, null);
            return new StoreLogisticsViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == ITEM_STORE) {
            StoreViewHolder storeViewHolder = (StoreViewHolder) viewHolder;
            storeViewHolder.setData(position);
        }
        if (getItemViewType(position) == ITEM_STORE_LOGISTICS) {
            StoreLogisticsViewHolder storeLogisticsViewHolder = (StoreLogisticsViewHolder) viewHolder;
            storeLogisticsViewHolder.setData(position);
        }
    }

    @Override
    public int getItemCount() {
        if (mLogisticsBean.getOrder_info() != null) {
            return mLogisticsBean.getOrder_info().size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(Utils.getBlanketOrderState(mLogisticsBean.getAll_info().getType())==OrderDetailActivity.WAIT_TO_ZHUHAI){
            return ITEM_STORE_LOGISTICS;
        }else {
            return ITEM_STORE;
        }
    }

    private class StoreViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_store_name;
        private Button bt_see_goods;
        private View view_line;

        public StoreViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_store_name = itemView.findViewById(R.id.tv_store_name);
            bt_see_goods = itemView.findViewById(R.id.bt_see_goods);
            view_line = itemView.findViewById(R.id.view_line);
        }

        public void setData(final int position) {
            tv_store_name.setText(mLogisticsBean.getOrder_info().get(position).getStore_name());
            bt_see_goods.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(mLogisticsBean.getOrder_info().get(position).getOrder_id())) {
                        Intent intent = new Intent(mContext, GoodsListActivity.class);
                        intent.putExtra(GoodsListActivity.KEY_ORDER_ID, mLogisticsBean.getOrder_info().get(position).getOrder_id());
                        mContext.startActivity(intent);
                    }
                }
            });
            if (position == getItemCount() - 1) {
                view_line.setVisibility(View.GONE);
            } else {
                view_line.setVisibility(View.VISIBLE);
            }
        }
    }

    private class StoreLogisticsViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_store_name, tv_done_to_zhuhai, tv_tracking_number, tv_logistics_company, tv_contact_people,tv_contact_people_title;
        private Button bt_see_goods, bt_confirm_warehouse;
        private View view_line;

        public StoreLogisticsViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_store_name = itemView.findViewById(R.id.tv_store_name);
            tv_done_to_zhuhai = itemView.findViewById(R.id.tv_done_to_zhuhai);
            tv_tracking_number = itemView.findViewById(R.id.tv_tracking_number);
            tv_logistics_company = itemView.findViewById(R.id.tv_logistics_company);
            tv_contact_people = itemView.findViewById(R.id.tv_contact_people);
            tv_contact_people_title = itemView.findViewById(R.id.tv_contact_people_title);
            bt_see_goods = itemView.findViewById(R.id.bt_see_goods);
            bt_confirm_warehouse = itemView.findViewById(R.id.bt_confirm_warehouse);
            view_line = itemView.findViewById(R.id.view_line);
        }

        public void setData(final int position) {
            tv_store_name.setText(mLogisticsBean.getOrder_info().get(position).getStore_name());
            if (mLogisticsBean.getOrder_info().get(position).getOrder_state() == 40) {
                tv_done_to_zhuhai.setVisibility(View.VISIBLE);
            } else {
                tv_done_to_zhuhai.setVisibility(View.GONE);
            }
            if(mLogisticsBean.getOrder_info().get(position).getOrder_state()==30
                    || mLogisticsBean.getOrder_info().get(position).getOrder_state()==40){
                tv_tracking_number.setText(mLogisticsBean.getOrder_info().get(position).getShipping_code());
                tv_logistics_company.setText(mLogisticsBean.getOrder_info().get(position).getE_name());
                if(TextUtils.isEmpty(mLogisticsBean.getOrder_info().get(position).getTelphone())){
                    tv_contact_people.setVisibility(View.GONE);
                    tv_contact_people_title.setVisibility(View.GONE);
                }else {
                    tv_contact_people.setVisibility(View.VISIBLE);
                    tv_contact_people_title.setVisibility(View.VISIBLE);
                    tv_contact_people.setText(mLogisticsBean.getOrder_info().get(position).getSeller_name()
                            + " "+mLogisticsBean.getOrder_info().get(position).getTelphone());
                }
            }else {
                tv_tracking_number.setText(R.string.logistics_empty);
                tv_logistics_company.setText(R.string.logistics_empty);
                tv_contact_people.setText(R.string.logistics_empty);
            }
            if (mLogisticsBean.getOrder_info().get(position).getOrder_state() == 30 &&
                    mLogisticsBean.isPower1()) {
                bt_confirm_warehouse.setVisibility(View.VISIBLE);
                bt_confirm_warehouse.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogUtils.createTwoBtnDialog(mContext
                                , mContext.getString(R.string.confirm_to_zhuhai_tip)
                                , mContext.getString(R.string.confirm)
                                , mContext.getString(R.string.cancel)
                                , new DialogUtils.OnRightBtnListener() {
                                    @Override
                                    public void setOnRightListener(Dialog dialog) {
                                        requestOrderToZhuhai(position);
                                    }
                                }, null, false, true);
                    }
                });
            } else {
                bt_confirm_warehouse.setVisibility(View.GONE);
            }
            if (position == getItemCount() - 1) {
                view_line.setVisibility(View.GONE);
            } else {
                view_line.setVisibility(View.VISIBLE);
            }
            bt_see_goods.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(mLogisticsBean.getOrder_info().get(position).getOrder_id())) {
                        Intent intent = new Intent(mContext, GoodsListActivity.class);
                        intent.putExtra(GoodsListActivity.KEY_ORDER_ID, mLogisticsBean.getOrder_info().get(position).getOrder_id());
                        ((Activity)mContext).startActivityForResult(intent, OrderDetailActivity.REQUEST_GOODS_CODE);
                    }
                }
            });

        }
    }

    private void requestOrderToZhuhai(int position) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_TO_ZHUHAI())
                .addParam("order_id", mLogisticsBean.getOrder_info().get(position).getOrder_id())
                .withPOST(new NetCallBack<String>(mContext) {
                    @Override
                    public void onSuccess(@NonNull String str) {
                        if(mListener!=null){
                            mListener.onRefreshOrderDetail();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Toast.makeText(mContext, err, Toast.LENGTH_SHORT).show();
                    }

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }
                }, false);
    }

    public interface OnRefreshOrderDetail{
        void onRefreshOrderDetail();
    }
}
