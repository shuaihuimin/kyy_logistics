package com.xht.logisticssystem.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xht.logisticssystem.R;
import com.xht.logisticssystem.bean.LogisiticsListBean;

import java.util.List;

public class LogisticsListAdapter extends BaseAdapter {
    private Context context;
    private List<LogisiticsListBean.ListBean> listBean;

    public LogisticsListAdapter(Context context, List<LogisiticsListBean.ListBean> listBean) {
        this.context = context;
        this.listBean = listBean;
    }

    @Override
    public int getCount() {
        if(listBean!=null && listBean.size()!=0){
            return listBean.size();
        }else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return listBean.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHodler;
        if(convertView==null){
            mViewHodler=new ViewHolder();
            convertView=View.inflate(context, R.layout.item_logistics_list_layout,null);
            mViewHodler.tv_order_numer=(TextView) convertView.findViewById(R.id.tv_order_numer);
            mViewHodler.tv_consignee=(TextView) convertView.findViewById(R.id.tv_consignee);
            mViewHodler.tv_tel=(TextView) convertView.findViewById(R.id.tv_tel);
            mViewHodler.tv_tel_number=(TextView) convertView.findViewById(R.id.tv_tel_number);
            mViewHodler.tv_phone=(TextView) convertView.findViewById(R.id.tv_phone);
            mViewHodler.tv_phone_number=(TextView) convertView.findViewById(R.id.tv_phone_number);
            mViewHodler.tv_mob=(TextView) convertView.findViewById(R.id.tv_mob);
            mViewHodler.tv_mob_number=(TextView) convertView.findViewById(R.id.tv_mob_number);
            mViewHodler.tv_shipping_address=(TextView) convertView.findViewById(R.id.tv_shipping_address);
            convertView.setTag(mViewHodler);
        }else {
            mViewHodler=(ViewHolder)convertView.getTag();
        }
        mViewHodler.tv_order_numer.setText(context.getResources().getString(R.string.order_number)
                + listBean.get(position).getOrder_main_sn());
        mViewHodler.tv_consignee.setText(listBean.get(position).getAddressee());
        if(!TextUtils.isEmpty(listBean.get(position).getTel())){
            mViewHodler.tv_tel.setVisibility(View.VISIBLE);
            mViewHodler.tv_tel_number.setVisibility(View.VISIBLE);
            mViewHodler.tv_tel_number.setText("+"+listBean.get(position).getTel());
        }else {
            mViewHodler.tv_tel.setVisibility(View.GONE);
            mViewHodler.tv_tel_number.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(listBean.get(position).getPhone())){
            mViewHodler.tv_phone.setVisibility(View.INVISIBLE);
            mViewHodler.tv_phone_number.setVisibility(View.VISIBLE);
            mViewHodler.tv_phone_number.setText("+"+listBean.get(position).getPhone());
        }else {
            mViewHodler.tv_phone.setVisibility(View.GONE);
            mViewHodler.tv_phone_number.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(listBean.get(position).getMob())){
            mViewHodler.tv_mob.setVisibility(View.INVISIBLE);
            mViewHodler.tv_mob_number.setVisibility(View.VISIBLE);
            mViewHodler.tv_mob_number.setText("+"+listBean.get(position).getMob());
        }else {
            mViewHodler.tv_mob.setVisibility(View.GONE);
            mViewHodler.tv_mob_number.setVisibility(View.GONE);
        }

        mViewHodler.tv_shipping_address.setText(listBean.get(position).getAddress());

        return convertView;
    }

    class ViewHolder {
        private TextView tv_order_numer;
        private TextView tv_consignee;
        private TextView tv_tel;
        private TextView tv_tel_number;
        private TextView tv_phone;
        private TextView tv_phone_number;
        private TextView tv_mob;
        private TextView tv_mob_number;
        private TextView tv_shipping_address;

    }
}
