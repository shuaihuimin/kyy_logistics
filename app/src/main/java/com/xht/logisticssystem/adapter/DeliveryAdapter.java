package com.xht.logisticssystem.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.utils.Utils;

import java.util.List;

public class DeliveryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private List<String> mImagePaths;
    private OnItemClickListener mListener;
    public static final int MAX_COUNT = 6;

    public DeliveryAdapter(Context context, List<String> imagePaths) {
        mContext = context;
        mImagePaths = imagePaths;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = View.inflate(mContext, R.layout.item_image, null);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((ViewHolder)viewHolder).setData(position);
    }

    @Override
    public int getItemCount() {
        if(mImagePaths.size()== MAX_COUNT+1){
            return MAX_COUNT;
        }
        return mImagePaths.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_img = itemView.findViewById(R.id.iv_img);
        }

        public void setData(final int position){
            iv_img.getLayoutParams().height = iv_img.getLayoutParams().width
                    = (Utils.getScreenWidth(mContext)- Utils.dp2px(mContext,9*2))/3-Utils.dp2px(mContext,3*2);
            if(TextUtils.isEmpty(mImagePaths.get(position))){
                Glide.with(mContext).load(R.mipmap.add_icon_picture).into(iv_img);
            }else {
                Glide.with(mContext).load((String) mImagePaths.get(position)).into(iv_img);
            }
            iv_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener!=null){
                        mListener.onItemClick(position,v);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(int position,View view);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }
}
