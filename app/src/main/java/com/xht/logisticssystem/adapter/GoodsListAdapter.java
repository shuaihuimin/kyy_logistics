package com.xht.logisticssystem.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.bean.GoodsListBean;
import com.xht.logisticssystem.utils.Utils;

public class GoodsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private GoodsListBean mGoodsListBean;

    public GoodsListAdapter(Context context,GoodsListBean goodsListBean) {
        mContext = context;
        mGoodsListBean = goodsListBean;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = View.inflate(mContext, R.layout.item_goods, null);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((ViewHolder)viewHolder).setData(position);
    }

    @Override
    public int getItemCount() {
        if(mGoodsListBean.getGoods()!=null){
            return mGoodsListBean.getGoods().size();
        }
        return 0;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_goods_img;
        private TextView tv_goods_name, tv_spec, tv_num;
        private View view_line;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_goods_img = itemView.findViewById(R.id.iv_goods_img);
            tv_goods_name = itemView.findViewById(R.id.tv_goods_name);
            tv_spec = itemView.findViewById(R.id.tv_spec);
            tv_num = itemView.findViewById(R.id.tv_num);
            view_line = itemView.findViewById(R.id.view_line);
        }

        public void setData(int position){
            Glide.with(mContext).load(mGoodsListBean.getGoods().get(position).getGoods_image())
                    .placeholder(R.mipmap.thumbnail_square)
                    .into(iv_goods_img);
            if(mGoodsListBean.getGoods().get(position).getIs_second_hand() == 1){
                tv_goods_name.setText(Utils.secondLabel(mGoodsListBean.getGoods().get(position).getGoods_name()));
            }else {
                tv_goods_name.setText(mGoodsListBean.getGoods().get(position).getGoods_name());
            }
            tv_spec.setText(mGoodsListBean.getGoods().get(position).getGoods_spec());
            tv_num.setText(mContext.getString(R.string.num)+mGoodsListBean.getGoods().get(position).getGoods_num());
            if(position==getItemCount()-1){
                view_line.setVisibility(View.GONE);
            }else {
                view_line.setVisibility(View.VISIBLE);
            }
        }
    }


}
