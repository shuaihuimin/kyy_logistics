package com.xht.logisticssystem.widget;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;

import com.xht.logisticssystem.R;


public class FullScreenDialog extends Dialog {
    public FullScreenDialog(@NonNull Context context) {
        this(context, R.style.FullScreenDialog_transparent);
    }

    public FullScreenDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setCanceledOnTouchOutside(false);
    }

}
