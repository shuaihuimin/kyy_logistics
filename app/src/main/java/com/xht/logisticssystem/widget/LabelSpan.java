package com.xht.logisticssystem.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.text.style.ReplacementSpan;

public class LabelSpan extends ReplacementSpan {
    private int bgColor;
    private int textColor;

    //宽
    private int mWidth;

    //字的宽高
    private int mTextWidth;
    private int mTextHeight;

    //背景的宽高
    private int mBgWidth;
    private int mBgHeight;
    private int textSize;

    private int padding;
    private int margin;

    public LabelSpan(int bgColor, int textColor, int textSize, int padding, int margin) {
        super();
        this.bgColor = bgColor;
        this.textColor = textColor;
        this.textSize = textSize;
        this.padding = padding;
        this.margin = margin;
    }

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        text = text.subSequence(start, end);
        Paint p = getCustomTextPaint(paint);

        //获取单纯的文字宽高，没有行间距的
        Rect rect = new Rect();
        paint.getTextBounds(text.toString(), 0, text.length(), rect);
        mTextWidth = rect.width();
        mTextHeight = rect.height();
        mBgWidth = (int) p.measureText(text.toString()) + padding*2;

        //宽等于背景加右边距
        mWidth = (int) p.measureText(text.toString()) + padding * 2 + margin;
        return mWidth;
    }


    /**
     * @param canvas
     * @param text
     * @param start  setSpan传进来的
     * @param end    setSpan传进来的
     * @param x      文字左下角x轴
     * @param top    当前span所在行的上方y
     * @param y      文字左下角y轴
     * @param bottom 当前span所在行的下方y(包含了行间距)，会和下一行的top重合
     * @param paint
     */
    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        //画背景
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(bgColor);
        Paint.FontMetricsInt fontMetrics = paint.getFontMetricsInt();
        canvas.drawRect(x, y+fontMetrics.ascent+fontMetrics.leading, x + mBgWidth, y+fontMetrics.descent, paint);

        paint.setColor(textColor);
        text = text.subSequence(start, end);
        Paint p = getCustomTextPaint(paint);
        canvas.drawText(text.toString(), x + (mBgWidth - mTextWidth) / 2, y, p);

    }

    private TextPaint getCustomTextPaint(Paint srcPaint) {
        TextPaint paint = new TextPaint(srcPaint);
        paint.setTextSize(textSize);   //设定字体大小
        paint.setFakeBoldText(true);
        return paint;
    }
}
