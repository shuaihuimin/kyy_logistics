package com.xht.logisticssystem.utils;

import android.util.Log;

import com.xht.logisticssystem.BuildConfig;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by 俊华 on 18/2/2.
 * 签名加密
 */

public class SignUtil {

    private static final String TAG = "SignUtil";

    /**
     * 签名
     * @param params 加密参数
     * @param t 加密时间戳
     * @return 加密后的数据
     */
    public static String sign(Map<String, Object> params, String t) {
        String content = "";
        String s1 = "";
        if (Login.Companion.getInstance().isLogin()) {//已登录
            if (params.isEmpty()) {//无参数
                Log.e(TAG, "map isEmpty : " + t + Login.Companion.getInstance().getToken_id());
                s1 = getMD5(getMD5(BuildConfig.APP_KEY) + t+ BuildConfig.APP_KEY
                        + Login.Companion.getInstance().getToken_id()
                        + Login.Companion.getInstance().getToken());
            } else {//有参数
                content = MapParamToString(params);
                s1 = getMD5(getMD5(content) + t+ BuildConfig.APP_KEY
                        + Login.Companion.getInstance().getToken_id()
                        + Login.Companion.getInstance().getToken());
                Log.e(TAG, "MapParamToString = " + content + "\n" + getMD5(content)
                        + "\n" + t + "\n" + Login.Companion.getInstance().getToken_id() + "\n");
            }
        } else {//未登录
            if (params.isEmpty()) {//无参数
                Log.e(TAG, "未登录 map isEmpty : " + t +"   ----   "+ BuildConfig.APP_KEY);
                s1 = getMD5(getMD5(BuildConfig.APP_KEY) + t + BuildConfig.APP_KEY);
            } else {//有参数
                content = MapParamToString(params);
                s1 = getMD5(getMD5(content) + t + BuildConfig.APP_KEY);
                Log.e(TAG, "未登录 MapParamToString   ------>  " + content + "\n" + getMD5(content)
                        + "\n" + t + "\n" + BuildConfig.APP_KEY + "\n");
            }
        }

        return s1;


    }


    /**
     * 处理传入的请求参数
     */
    private static String MapParamToString(Map<String, Object> params) {
        params = sortMapByKey(params);
        StringBuilder sb = new StringBuilder();
        if (params != null) {
            if (params.isEmpty()) {
                return sb.toString();
            }
            Iterator<Map.Entry<String, Object>> i = params.entrySet().iterator();
            for (; ; ) {
                Map.Entry<String, Object> m = i.next();
                String key = m.getKey();
                Object value = m.getValue();
                sb.append(key);
                sb.append('=');
                sb.append(value);
                if (!i.hasNext()) {
                    return sb.toString();
                }
                sb.append('&');
            }

        }
        return sb.toString();
    }

    /**
     * 进行md5加密
     * @param content
     * @return
     */
    public static String getMD5(String content) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(content.getBytes());
            return getHashString(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * 获取HashString
     * @param digest
     * @return
     */
    private static String getHashString(MessageDigest digest) {
        StringBuilder builder = new StringBuilder();
        for (byte b : digest.digest()) {
            builder.append(Integer.toHexString((b >> 4) & 0xf));
            builder.append(Integer.toHexString(b & 0xf));
        }
        Log.e(TAG,"getHashString() "+ builder.toString());
        return builder.toString();
    }

    /**
     * 使用 Map按key进行排序
     *
     * @param map
     * @return
     */
    public static Map<String, Object> sortMapByKey(Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }

        Map<String, Object> sortMap = new TreeMap<String, Object>(
                new MapKeyComparator());

        sortMap.putAll(map);

        Log.e(TAG,"排序 sortMapByKey"+ sortMap.toString());
        return sortMap;
    }

    static class MapKeyComparator implements Comparator<String> {

        @Override
        public int compare(String str1, String str2) {

            return str1.compareTo(str2);
        }
    }

}
