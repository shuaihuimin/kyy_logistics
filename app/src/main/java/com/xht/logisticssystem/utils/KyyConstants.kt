package com.xht.logisticssystem.utils

import com.xht.logisticssystem.BuildConfig


object KyyConstants {
    val URL_CONTEXTPATH = BuildConfig.DOMAIN + "index/"

    /**
     * 分页
     */
    val URL_LOAD = URL_CONTEXTPATH + "act=xht_kyy&op=index"

    val URL_LOGIN = URL_CONTEXTPATH + "app/token"

    val URL_LOGIN_OUT = URL_CONTEXTPATH + "app/quit_login"

    val URL_LOGISTICS_INFO = URL_CONTEXTPATH + "app_logistic/find"

    val URL_GOODS_LIST = URL_CONTEXTPATH + "app_logistic/order_info"

    //子订单确认到仓（珠海）
    val URL_ORDER_TO_ZHUHAI = URL_CONTEXTPATH + "app_logistic/order_fetch"

    //订单详情底下的按钮接口
    val URL_ORDER_LOGISTICS_CHANGE = URL_CONTEXTPATH + "app_logistic/logistics_change"

    //版本更新
    val URL_UPADATE_VERSION = URL_CONTEXTPATH + "app/version"

    //物流订单列表
    val URL_LOGISTICS_LIST= URL_CONTEXTPATH+"app_logistic/logistics_list"

    //查看更多
    val URL_LODE= URL_CONTEXTPATH+"app/page"

    val URL_UPLOAD_IMG= URL_CONTEXTPATH+"app/upload_multi"

}
