package com.xht.logisticssystem.utils


import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by 俊华 on 2018/1/15 0015.
 * 通用系统工具
 */
object SystemUtil {

    // 将字符串转为时间戳
    fun getTime(user_time: String): String? {
        var re_time: String? = null
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val d: Date

        try {
            d = sdf.parse(user_time)
            val l = d.time
            val str = l.toString()
            re_time = str.substring(0, 10)
        } catch (e: java.text.ParseException) {
            e.printStackTrace()
        }

        return re_time
    }

    // 将时间戳 转为字符串
    fun getTimeStr(user_time: String = "${System.currentTimeMillis()}"): String {


        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        if (user_time.length == 10) {
            val date = Date(user_time.toLong() * 1000)
            return simpleDateFormat.format(date)
        } else if (user_time.length == 13) {
            val date = Date(user_time.toLong())
            return simpleDateFormat.format(date)
        }


        return ""
    }


}