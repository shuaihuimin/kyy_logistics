package com.xht.logisticssystem.utils;

import android.util.Log;

import com.xht.logisticssystem.BuildConfig;


public class LogUtil {
    public static void e(String tag, String text) {
        if (BuildConfig.DEBUG) {
            if (text.length() > 4000) {
                for (int i = 0; i < text.length(); i += 4000) {
                    if (i + 4000 < text.length())
                        Log.e(tag, text.substring(i, i + 4000));
                    else {
                        Log.e(tag, text.substring(i, text.length()));
                    }
                }
            } else {
                Log.e(tag, text);
            }
        }
    }

    public static void i(String tag, String text) {
        if (BuildConfig.DEBUG) {
            if (text.length() > 4000) {
                for (int i = 0; i < text.length(); i += 4000) {
                    if (i + 4000 < text.length())
                        Log.i(tag, text.substring(i, i + 4000));
                    else {
                        Log.i(tag, text.substring(i, text.length()));
                    }
                }
            } else {
                Log.i(tag, text);
            }
        }
    }

}
