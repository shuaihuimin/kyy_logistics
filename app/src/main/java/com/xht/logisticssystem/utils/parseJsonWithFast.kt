package com.xht.logisticssystem.utils

import android.text.TextUtils
import com.alibaba.fastjson.JSONObject

/**
 * Created by pjh on 18/3/9.
 */
object JsonU {

    /**
     * 处理自定义的类
     */
    fun <T> parseJsonWithFast(jsonData: String?, type: Class<T>?): T? {
        if (TextUtils.isEmpty(jsonData)) {
            return null
        }
        try {
            LogUtil.e("数据类型为自定义", "parseJsonWithFast()")
            return JSONObject.parseObject(jsonData, type)
        } catch (e: Exception) {
            LogUtil.e("fastJson解析数据异常", e.toString())
            return null
        }
    }
}