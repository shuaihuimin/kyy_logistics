package com.xht.logisticssystem.utils;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.xht.logisticssystem.BuildConfig;
import com.xht.logisticssystem.KyyLogisticsApp;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.activity.OrderDetailActivity;
import com.xht.logisticssystem.bean.LogisiticsListBean;
import com.xht.logisticssystem.widget.LabelSpan;

import java.lang.reflect.Method;


public class Utils {

    /**
     * 判断是否是emui系统，emui系统是华为的定制系统
     *
     * @return
     */
    public static boolean isHUAWEI() {
        int emuiApiLevel = 0;
        try {
            Class cls = Class.forName("android.os.SystemProperties");
            Method method = cls.getDeclaredMethod("get", new Class[]{String.class});
            emuiApiLevel = Integer.parseInt((String) method.invoke(cls, new Object[]{"ro.build.hw_emui_api_level"}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emuiApiLevel > 0;
    }


    /**
     * @param type 总订单的状态
     * @return 总订单显示的状态
     */
    public static int getBlanketOrderState(int type) {
        if (type == 0 || type == 1 || type == 2 || type == 10) {
            return OrderDetailActivity.WAIT_TO_ZHUHAI;
        }
        if (type == 11) {
            return OrderDetailActivity.WAIT_CLEARANCE_OF_GOODS;
        }
        if (type == 20) {
            return OrderDetailActivity.WAIT_TO_MACAU;
        }
        if (type == 30) {
            return OrderDetailActivity.WAIT_TO_DELIVERY;
        }
        if (type == 40) {
            return OrderDetailActivity.WAIT_SIGN_IN;
        }
        if (type == 48) {
            return OrderDetailActivity.DONE;
        }
        return 0;
    }

    /**
     * convert dp to its equivalent px
     * <p>
     * 将dp转换为与之相等的px
     */
    public static int dp2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    /**
     * 获得屏幕宽度
     *
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    /**
     * 标题加二手标签
     */
    public static SpannableString secondLabel(String title) {
        SpannableString spannableString = new SpannableString(KyyLogisticsApp.context.getString(R.string.second_hand) + title);
        spannableString.setSpan(
                new LabelSpan(KyyLogisticsApp.context.getResources().getColor(R.color.green)
                        , Color.WHITE, Utils.sp2px(KyyLogisticsApp.context, 11)
                        , Utils.dp2px(KyyLogisticsApp.context, 7)
                        , Utils.dp2px(KyyLogisticsApp.context, 5)), 0
                , 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static int sp2px(Context context, float spValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    public static void setIconBadgeNum(LogisiticsListBean.numBean _numBean){
        //不支持角标会崩溃，所以需要try catch
        try{
            int num = _numBean.getWait_first()
                    + _numBean.getWait_second()
                    + _numBean.getWait_third()
                    + _numBean.getWait_forth()
                    + _numBean.getWait_fifth();
            Bundle bunlde =new Bundle();
            bunlde.putString("package", BuildConfig.APPLICATION_ID);
            bunlde.putString("class", "com.xht.logisticssystem.activity.SplashActivity");
            bunlde.putInt("badgenumber",num);
            if(KyyLogisticsApp.context!=null){
                KyyLogisticsApp.context.getContentResolver()
                        .call(Uri.parse("content://com.huawei.android.launcher.settings/badge/")
                                , "change_badge", null, bunlde);
            }
        }catch(Exception e){
        }
    }
}
