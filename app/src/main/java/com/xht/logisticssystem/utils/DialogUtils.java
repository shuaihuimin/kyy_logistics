package com.xht.logisticssystem.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.xht.logisticssystem.R;
import com.xht.logisticssystem.widget.FullScreenDialog;

public class DialogUtils {

    /**
     *
     * @param context 上下文，不能是ApplicationContext
     * @param message 显示文字
     */
    public static void createTipAllTextDialog(Context context,String message){
        final FullScreenDialog dialog = new FullScreenDialog(context);
        dialog.show();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_tip_all_text,null);
        TextView tv_tip = view.findViewById(R.id.tv_tip);
        tv_tip.setText(message);
        dialog.setContentView(view);
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        },1000);

    }

    /**
     *
     * @param context  上下文
     * @param message 显示内容
     * @param rightBtnName 右边按钮的文字
     * @param leftBtnName  左边按钮的文字
     * @param onRightBtnListener  右边按钮的点击事件
     * @param onLeftBtnListener   左边按钮的点击事件
     * @param canceledOnTouchOutside 点击外面是否能取消
     * @param cancelable 点击返回键是否能取消
     * @return
     */
    public static void createTwoBtnDialog(Context context, String message,
                                          String rightBtnName, String leftBtnName,
                                          final OnRightBtnListener onRightBtnListener,
                                          final OnLeftBtnListener onLeftBtnListener,
                                          boolean canceledOnTouchOutside, boolean cancelable){
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        dialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_twobtn,null);
        TextView tv_content = view.findViewById(R.id.tv_content);
        Button bt_left = view.findViewById(R.id.bt_left);
        Button bt_right = view.findViewById(R.id.bt_right);
        tv_content.setText(message);
        bt_left.setText(leftBtnName);
        bt_right.setText(rightBtnName);
        final Dialog finalDialog = dialog;
        dialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
        dialog.setCancelable(cancelable);
        bt_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onLeftBtnListener!=null){
                    onLeftBtnListener.setOnLeftListener(finalDialog);
                }
                finalDialog.dismiss();
            }
        });
        bt_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onRightBtnListener!=null){
                    onRightBtnListener.setOnRightListener(finalDialog);
                }
                finalDialog.dismiss();
            }
        });
        dialog.show();
        //setContentView一定要放在show后面才行显示出页面
        dialog.setContentView(view);
    }

    /**
     *
     * @param context  上下文
     * @param message 显示内容
     * @param btnName  按钮的文字
     * @param onLeftBtnListener   按钮的点击事件
     * @param canceledOnTouchOutside 点击外面是否能取消
     * @param cancelable 点击返回键是否能取消
     * @return
     */
    public static void createOneBtnDialog(Context context, String message,
                                          String btnName,
                                          final OnLeftBtnListener onLeftBtnListener,
                                          boolean canceledOnTouchOutside,boolean cancelable){
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        dialog = builder.create();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_onebtn,null);
        TextView tv_content = view.findViewById(R.id.tv_content);
        TextView tv_sure = view.findViewById(R.id.tv_sure);
        tv_content.setText(message);
        tv_sure.setText(btnName);
        final Dialog finalDialog = dialog;
        dialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
        dialog.setCancelable(cancelable);
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onLeftBtnListener!=null){
                    onLeftBtnListener.setOnLeftListener(finalDialog);
                }
                finalDialog.dismiss();
            }
        });
        dialog.show();
        //setContentView一定要放在show后面才行显示出页面
        dialog.setContentView(view);
    }

    public interface OnRightBtnListener {
        void setOnRightListener(Dialog dialog);
    }
    public interface OnLeftBtnListener {
        void setOnLeftListener(Dialog dialog);
    }

}
