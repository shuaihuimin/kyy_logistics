package com.xht.logisticssystem.activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bilibili.boxing.Boxing;
import com.bilibili.boxing.model.config.BoxingConfig;
import com.bilibili.boxing.model.entity.BaseMedia;
import com.bilibili.boxing_impl.ui.BoxingActivity;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.adapter.DeliveryAdapter;
import com.xht.logisticssystem.fragment.QueryFragment;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Login;
import com.xht.logisticssystem.utils.SignUtil;
import com.xht.logisticssystem.utils.SystemUtil;
import com.xht.logisticssystem.widget.FullScreenDialog;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import top.zibin.luban.CompressionPredicate;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;
import top.zibin.luban.OnRenameListener;

public class DeliveryActivity extends BaseActivity implements DeliveryAdapter.OnItemClickListener {

    private ImageView iv_back;
    private TextView tv_title, tv_tip;
    private Button bt_confirm_delivery;
    private RecyclerView rv_content;

    public final static String KEY_TYPE = "type";
    public final static String KEY_ORDER_MAIN_ID = "order_main_id";
    public final static String KEY_IMGS = "key_imgs";
    private static final int REQUEST_SELECT_CODE = 1024;
    private static final int REQUEST_LOOK_CODE = 1025;
    public final static int TYPE_UPLOAD = 0;//上传送货单
    public final static int TYPE_LOOK = 1;//查看送货单
    private int mType = 0;
    private int mOrderMainId;
    private ArrayList<String> mImagePaths = new ArrayList<>();//选中图片的路径集合
    private ArrayList<String> mCompressImagePaths;//上传前压缩好的路径集合
    private DeliveryAdapter mAdapter;

    private FullScreenDialog mLoadingDialog;

    @Override
    public int setLayoutResId() {
        return R.layout.activity_delivery;
    }

    @Override
    public void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        tv_tip = findViewById(R.id.tv_tip);
        rv_content = findViewById(R.id.rv_content);
        bt_confirm_delivery = findViewById(R.id.bt_confirm_delivery);
        iv_back.setVisibility(View.VISIBLE);
        mType = getIntent().getIntExtra(KEY_TYPE, 0);
        mOrderMainId = getIntent().getIntExtra(KEY_ORDER_MAIN_ID, 0);
        if (mType == TYPE_UPLOAD) {
            mImagePaths.add("");
            tv_title.setText(R.string.confirm_delivery);
            bt_confirm_delivery.setVisibility(View.VISIBLE);
            notifyDataChange();
            bt_confirm_delivery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogUtils.createTwoBtnDialog(DeliveryActivity.this
                            , MessageFormat.format(getString(R.string.order_handle_confirm_tip)
                                    , getResources().getString(R.string.confirm_delivery))
                            , getString(R.string.confirm)
                            , getString(R.string.cancel)
                            , new DialogUtils.OnRightBtnListener() {
                                @Override
                                public void setOnRightListener(Dialog dialog) {
                                    showLoadingDialog();
                                    compress();
                                }
                            }, null, false, true);
                }
            });
        } else {
            tv_title.setText(R.string.delivery_note);
            bt_confirm_delivery.setVisibility(View.GONE);
            tv_tip.setVisibility(View.GONE);
            mImagePaths = getIntent().getStringArrayListExtra(KEY_IMGS);
            if(mImagePaths!=null){
                notifyDataChange();
            }
        }
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showLoadingDialog() {
        if(mLoadingDialog ==null){
            mLoadingDialog = new FullScreenDialog(this);
            mLoadingDialog.setCancelable(true);
            mLoadingDialog.setCanceledOnTouchOutside(false);
            mLoadingDialog.show();
            ProgressBar progressBar = new ProgressBar(this);
            mLoadingDialog.setContentView(progressBar);
        }else {
            mLoadingDialog.show();
        }
    }

    private void compress() {
        mCompressImagePaths = new ArrayList<>();
        final ArrayList<String> pathList = new ArrayList<>();
        for (int i = 0; i < mImagePaths.size() - 1; i++) {
            pathList.add(mImagePaths.get(i).replace("file://", ""));
        }
        Luban.with(this)
                .load(pathList)
                .ignoreBy(200)
                .setTargetDir(getCacheDir().getAbsolutePath())
                .setFocusAlpha(false)
                .filter(new CompressionPredicate() {
                    @Override
                    public boolean apply(String path) {
                        return !TextUtils.isEmpty(path);
                    }
                })
                .setRenameListener(new OnRenameListener() {
                    @Override
                    public String rename(String filePath) {
                        try {
                            MessageDigest md = MessageDigest.getInstance("MD5");
                            md.update(filePath.getBytes());
                            return new BigInteger(1, md.digest()).toString(32) + new File(filePath).getName();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                        return "";
                    }
                })
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onSuccess(File file) {
                        mCompressImagePaths.add(file.getPath());
                        if (mCompressImagePaths.size() == pathList.size()) {
                            requestUploadImg();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mLoadingDialog.dismiss();
                    }
                }).launch();

    }

    private void requestUploadImg() {
        String t = SystemUtil.INSTANCE.getTime(SystemUtil.INSTANCE.
                getTimeStr("" + System.currentTimeMillis()));
        Map<String, Object> map = new HashMap<>();
        map.put("device_uuid", NetUtil.Companion.getUuid());
        map.put("device_name", NetUtil.Companion.getDevice_name());
        map.put("order_main_id", mOrderMainId + "");
        NetUtil netUtil = NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPLOAD_IMG())
                .addParam("device_uuid", NetUtil.Companion.getUuid())
                .addParam("device_name", NetUtil.Companion.getDevice_name())
                .addParam("token_id", Login.Companion.getInstance().getToken_id())
                .addParam("sign_time", t)
                .addParam("sign", SignUtil.sign(map, t))
                .addParam("order_main_id", mOrderMainId + "");
        for (int i = 0; i < mImagePaths.size() - 1; i++) {
            File file = new File(mCompressImagePaths.get(i));
            netUtil.addParam(file.getName(), file);
        }
        netUtil.withPOSTFile(new NetCallBack<String>(this) {
            @NotNull
            @Override
            public Class<String> getRealType() {
                return String.class;
            }

            @Override
            public void onSuccess(@NonNull String result) {
                EventBus.getDefault().post(QueryFragment.REFRESH_LOGISTICS_LIST);
                if(isFinishing()){
                    return;
                }
                mLoadingDialog.dismiss();
                Intent intent = new Intent(DeliveryActivity.this,DoSuccesfulActivity.class);
                startActivity(intent);
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                if(isFinishing()){
                    return;
                }
                mLoadingDialog.dismiss();
                Toast.makeText(DeliveryActivity.this, err, Toast.LENGTH_SHORT).show();
            }
        }, false);
    }

    private void notifyDataChange() {
        if (mImagePaths.contains("") && mImagePaths.size() == 1 && mType==TYPE_UPLOAD) {
            tv_tip.setVisibility(View.VISIBLE);
            bt_confirm_delivery.setEnabled(false);
        } else {
            tv_tip.setVisibility(View.GONE);
            bt_confirm_delivery.setEnabled(true);
        }
        mAdapter = new DeliveryAdapter(this, mImagePaths);
        mAdapter.setOnItemClickListener(this);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        rv_content.setAdapter(mAdapter);
        rv_content.setLayoutManager(layoutManager);
    }

    @Override
    public void onItemClick(int position, View v) {
        if (TextUtils.isEmpty(mImagePaths.get(position))) {
            BoxingConfig config = new BoxingConfig(BoxingConfig.Mode.MULTI_IMG)
                    .withMaxCount(DeliveryAdapter.MAX_COUNT - mImagePaths.size() + 1)
                    .needCamera(R.mipmap.album_icon_photograph);
            Boxing.of(config).withIntent(this, BoxingActivity.class).start(this, REQUEST_SELECT_CODE);
        } else {
            Intent intent = new Intent(this, PhotoViewActivity.class);
            intent.putStringArrayListExtra(PhotoViewActivity.KEY_IMGS, mImagePaths);
            intent.putExtra(PhotoViewActivity.KEY_POSITION, position);
            if(mType==TYPE_UPLOAD){
                intent.putExtra(PhotoViewActivity.IS_SHOW_DELETE, true);
            }else {
                intent.putExtra(PhotoViewActivity.IS_SHOW_DELETE, false);
            }
            startActivityForResult(intent, REQUEST_LOOK_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SELECT_CODE && resultCode == RESULT_OK && mType == TYPE_UPLOAD) {
            ArrayList<BaseMedia> images = Boxing.getResult(data);
            if (images != null) {
                for (BaseMedia image : images) {
                    mImagePaths.add(0, "file://" + image.getPath());
                }
            }
            notifyDataChange();
        }

        if (requestCode == REQUEST_LOOK_CODE && resultCode == RESULT_OK) {
            mImagePaths = data.getStringArrayListExtra(PhotoViewActivity.KEY_IMGS);
            notifyDataChange();
        }
    }

    @Override
    public void onBackPressed() {
        if(mType==TYPE_UPLOAD&&mImagePaths!=null&&mImagePaths.size()>1){
            DialogUtils.createTwoBtnDialog(DeliveryActivity.this
                    , getResources().getString(R.string.delivery_back_tip)
                    , getString(R.string.confirm)
                    , getString(R.string.cancel)
                    , new DialogUtils.OnRightBtnListener() {
                        @Override
                        public void setOnRightListener(Dialog dialog) {
                            DeliveryActivity.super.onBackPressed();
                        }
                    }, null, false, true);
        }else {
            super.onBackPressed();
        }

    }
}
