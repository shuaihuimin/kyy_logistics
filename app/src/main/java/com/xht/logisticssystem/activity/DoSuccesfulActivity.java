package com.xht.logisticssystem.activity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xht.logisticssystem.R;

public class DoSuccesfulActivity extends BaseActivity {

    private ImageView iv_back;
    private TextView tv_title;
    private Button bt_back;

    @Override
    public int setLayoutResId() {
        return R.layout.activity_do_successful;
    }


    @Override
    public void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        bt_back = findViewById(R.id.bt_back);
        iv_back.setVisibility(View.VISIBLE);
        tv_title.setText(R.string.app_name);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


}
