package com.xht.logisticssystem.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置屏幕是竖屏
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(setLayoutResId());
        initView();
    }

    /**
     * @return 布局resId
     */
    public abstract int setLayoutResId();

    /**
     * 初始化,findViewById等操作
     */
    public abstract void initView();

}
