package com.xht.logisticssystem.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.xht.logisticssystem.KyyLogisticsApp;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.bean.LogisticsBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.LogUtil;
import com.xht.logisticssystem.utils.Login;

import org.jetbrains.annotations.NotNull;


/**
 * 华为推送点击通知都是打开这个activity然后再跳转的
 * <p>
 * 自定义动作的内容格式（后端使用到）：
 * intent:#Intent;launchFlags=0x10000000;component=com.xht.logisticssystem/com.xht.logisticssystem.activity.HuaweiPushActivity;S.ext1=helloExt1;S.ext2=helloExt2;end
 */
public class HuaweiPushActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huaweipush);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void requestLogistics(String code) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGISTICS_INFO())
                .addParam("sn_or_shipping", code)
                .withPOST(new NetCallBack<LogisticsBean>(this) {
                    @Override
                    public void onSuccess(@NonNull LogisticsBean logisticsBean) {
                        Intent intent = new Intent(HuaweiPushActivity.this, OrderDetailActivity.class);
                        intent.putExtra(OrderDetailActivity.KEY_ORDERDETAIL, logisticsBean);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if (errCode == 30000) {
                            //运单号或订单号不存在
                            Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
                        } else if (errCode == 30001) {
                            //无此订单物流
                            Intent intent = new Intent(HuaweiPushActivity.this, EmptyInfoActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    }

                    @NotNull
                    @Override
                    public Class<LogisticsBean> getRealType() {
                        return LogisticsBean.class;
                    }
                }, false);
    }

    private void handleIntent(Intent handleIntent) {
        LogUtil.i("huaweipush", handleIntent.toURI());
        Bundle bundle = handleIntent.getExtras();
        String order_main_id = "";
        String u_id = "";
        Intent intent = new Intent();

        if (Login.Companion.getInstance().isLogin()) {
            if (bundle != null) {
                u_id = bundle.getString("u_id");
                if (TextUtils.isEmpty(u_id) || !u_id.equals(Login.Companion.getInstance().getToken_id())) {
                    if (KyyLogisticsApp.activityNum <= 1) {
                        intent.setClass(this, MainActivity.class);
                    }
                } else {
                    order_main_id = bundle.getString("shipping_code");
                    if (TextUtils.isEmpty(order_main_id)) {
                        if (KyyLogisticsApp.activityNum <= 1) {
                            intent.setClass(this, MainActivity.class);
                        }
                    } else {
                        requestLogistics(order_main_id);
                        return;
                    }
                }
            } else {
                if (KyyLogisticsApp.activityNum <= 1) {
                    intent.setClass(this, MainActivity.class);
                }
            }
        } else {
            intent.setClass(this, LoginActivity.class);
        }

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        finish();
    }
}
