package com.xht.logisticssystem.activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.huawei.android.hms.agent.HMSAgent;
import com.huawei.android.hms.agent.common.handler.ConnectHandler;
import com.huawei.android.hms.agent.push.handler.GetTokenHandler;
import com.xht.logisticssystem.BuildConfig;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.bean.UpdateVersionBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Login;
import com.xht.logisticssystem.utils.Utils;

import org.jetbrains.annotations.NotNull;

public class SplashActivity extends BaseActivity {

    private ImageView iv_splash;

    @Override
    public int setLayoutResId() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_splash;
    }

    @Override
    public void initView() {
        iv_splash = findViewById(R.id.iv_splash);
        requestCheckVersion();
        //请求华为推送token
        if(Utils.isHUAWEI()){
            HMSAgent.connect(this, new ConnectHandler() {
                @Override
                public void onConnect(int rst) {
                    HMSAgent.Push.getToken(new GetTokenHandler() {
                        @Override
                        public void onResult(int rst) {

                        }
                    });
                }
            });
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void requestCheckVersion() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_UPADATE_VERSION())
                .addParam("version", BuildConfig.VERSION_NAME)
                .withPOST(new NetCallBack<UpdateVersionBean>(this) {

                    @NotNull
                    @Override
                    public Class<UpdateVersionBean> getRealType() {
                        return UpdateVersionBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        next();
                    }

                    @Override
                    public void onSuccess(@NonNull final UpdateVersionBean updateVersionBean) {
                        if (updateVersionBean.getRes() != null) {
                            final boolean isForceUpdate = updateVersionBean.getRes().isForceUpdate();
                            if (updateVersionBean.getRes().isIs_update()) {
                                String leftButtonText;
                                if (isForceUpdate) {
                                    leftButtonText = getString(R.string.logout);
                                } else {
                                    leftButtonText = getString(R.string.cancel);
                                }
                                DialogUtils.createTwoBtnDialog(SplashActivity.this
                                        , updateVersionBean.getRes().getUpgradePoint()
                                        , getString(R.string.confirm)
                                        , leftButtonText, new DialogUtils.OnRightBtnListener() {
                                            @Override
                                            public void setOnRightListener(Dialog dialog) {
                                                Uri uri = Uri.parse(updateVersionBean.getRes().getInstallPackage());
                                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                                if(intent.resolveActivity(getPackageManager())!=null){
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            }
                                        }, new DialogUtils.OnLeftBtnListener() {
                                            @Override
                                            public void setOnLeftListener(Dialog dialog) {
                                                if (isForceUpdate) {
                                                    finish();
                                                } else {
                                                    next();
                                                }
                                            }
                                        }, false, false);
                                return;
                            }
                        }
                        next();
                    }
                }, false);
    }

    public void next() {
        iv_splash.postDelayed(new Runnable() {

            @Override
            public void run() {
                /**
                 * 切换为非全屏
                 */
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                //版本升级，增加了华为推送需要,以前旧版本的用户需要重新登录注册华为推送
                if(Login.Companion.getInstance().getNeed_login_for_register_huaweipush().equals("1")){
                    Login.Companion.getInstance().setToken("");
                    Login.Companion.getInstance().setToken_id("");
                    Login.Companion.getInstance().setLogistics_person_id("");
                    Login.Companion.getInstance().setNeed_login_for_register_huaweipush("0");
                }
                if (TextUtils.isEmpty(Login.Companion.getInstance().getToken())) {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
                finish();


            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {

    }
}
