package com.xht.logisticssystem.activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bilibili.boxing_impl.view.HackyViewPager;
import com.bumptech.glide.Glide;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.utils.DialogUtils;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;

public class PhotoViewActivity extends BaseActivity {
    private TextView tv_right;
    private TextView tv_title;
    private ImageView iv_back;
    private HackyViewPager vp_content;

    public final static String KEY_IMGS = "key_imgs";
    public final static String KEY_POSITION = "key_position";
    public final static String IS_SHOW_DELETE = "key_is_show_delete";
    private ArrayList<String> mImgPaths;
    private int mPosition;
    private PhotoAdapter mAdapter;
    private boolean isDeleted;//是否删除过图片
    private boolean is_show_delete_btn;

    @Override
    public int setLayoutResId() {
        return R.layout.activity_photoview;
    }

    @Override
    public void initView() {
        tv_title = findViewById(R.id.tv_title);
        tv_right = findViewById(R.id.tv_right);
        iv_back = findViewById(R.id.iv_back);
        vp_content = findViewById(R.id.vp_content);

        tv_right.setText(R.string.delete);
        is_show_delete_btn = getIntent().getBooleanExtra(IS_SHOW_DELETE,false);
        if(is_show_delete_btn){
            tv_right.setVisibility(View.VISIBLE);
        }else {
            tv_right.setVisibility(View.GONE);
        }
        iv_back.setVisibility(View.VISIBLE);
        mImgPaths = getIntent().getStringArrayListExtra(KEY_IMGS);
        mPosition = getIntent().getIntExtra(KEY_POSITION, 0);
        setTitle();
        mAdapter = new PhotoAdapter();
        vp_content.setAdapter(mAdapter);
        vp_content.setCurrentItem(mPosition);
        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(PhotoViewActivity.this
                        , getString(R.string.confirm_delete_tip)
                        , getString(R.string.confirm)
                        , getString(R.string.cancel)
                        , new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                isDeleted = true;
                                mImgPaths.remove(mPosition);
                                if (mPosition == mImgPaths.size()) {
                                    mPosition = mPosition - 1;
                                }
                                if (mImgPaths.size() == 1) {
                                    onBackPressed();
                                    return;
                                }
                                mAdapter.notifyDataSetChanged();
                                setTitle();
                            }
                        }, null, false, true);
            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        vp_content.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPosition = position;
                setTitle();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setActivityResult() {
        Intent intent = new Intent();
        intent.putStringArrayListExtra(KEY_IMGS, mImgPaths);
        setResult(RESULT_OK, intent);
    }

    @Override
    public void onBackPressed() {
        if (isDeleted) {
            setActivityResult();
        }
        super.onBackPressed();

    }

    private void setTitle() {
        if(is_show_delete_btn){
            tv_title.setText((mPosition + 1) + "/" + (mImgPaths.size() - 1));
        }else {
            tv_title.setText((mPosition + 1) + "/" + mImgPaths.size());
        }
    }

    class PhotoAdapter extends PagerAdapter {
        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            if(is_show_delete_btn){
                return mImgPaths.size() - 1;
            }
            return mImgPaths.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            PhotoView photoView = new PhotoView(container.getContext());
            Glide.with(container.getContext()).load(mImgPaths.get(position)).into(photoView);
            container.addView(photoView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            return photoView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}
