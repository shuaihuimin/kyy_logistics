package com.xht.logisticssystem.activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.logisticssystem.KyyLogisticsApp;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.adapter.OrderDetailAdapter;
import com.xht.logisticssystem.bean.LogisticsBean;
import com.xht.logisticssystem.fragment.QueryFragment;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.text.MessageFormat;

public class OrderDetailActivity extends BaseActivity {

    private ImageView iv_back, iv_blanket_order_status;
    private TextView tv_title, tv_blanket_order_status, tv_order_number, tv_receive_name, tv_receive_phone, tv_receive_adddress;
    private RecyclerView rv_content;
    private Button bt_handle;
    private RelativeLayout rl_bottom_button;
    private LogisticsBean mLogisticsBean;

    public static final String KEY_ORDERDETAIL = "key_order_detail";
    public static final int REQUEST_GOODS_CODE = 12300;
    public static final int REQUEST_DELIVERY_CODE = 12301;

    //总订单的状态
    public static final int WAIT_TO_ZHUHAI = 0;//待入珠海仓
    public static final int WAIT_CLEARANCE_OF_GOODS = 1;//待报关
    public static final int WAIT_TO_MACAU = 2;//待入澳门仓
    public static final int WAIT_TO_DELIVERY = 3;//待配送
    public static final int WAIT_SIGN_IN = 4;//待签收
    public static final int DONE = 5;//已完成

    @Override
    public int setLayoutResId() {
        return R.layout.activity_order_detail;
    }

    @Override
    public void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        iv_blanket_order_status = findViewById(R.id.iv_blanket_order_status);
        tv_blanket_order_status = findViewById(R.id.tv_blanket_order_status);
        tv_order_number = findViewById(R.id.tv_order_number);
        tv_receive_name = findViewById(R.id.tv_receive_name);
        tv_receive_phone = findViewById(R.id.tv_receive_phone);
        tv_receive_adddress = findViewById(R.id.tv_receive_adddress);
        rv_content = findViewById(R.id.rv_content);
        bt_handle = findViewById(R.id.bt_handle);
        rl_bottom_button = findViewById(R.id.rl_bottom_button);

        iv_back.setVisibility(View.VISIBLE);
        tv_title.setText(R.string.order_detail);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mLogisticsBean = (LogisticsBean) getIntent().getSerializableExtra(KEY_ORDERDETAIL);
        showData();
    }

    private void showData() {
        final int blanketOrderState = Utils.getBlanketOrderState(mLogisticsBean.getAll_info().getType());
        switch (blanketOrderState) {
            case WAIT_TO_ZHUHAI:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_warehouse);
                tv_blanket_order_status.setText(R.string.wait_to_zhuhai);
                break;
            case WAIT_CLEARANCE_OF_GOODS:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_customs);
                tv_blanket_order_status.setText(R.string.wait_clearance_of_goods);
                break;
            case WAIT_TO_MACAU:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_warehouse);
                tv_blanket_order_status.setText(R.string.wait_to_macau);
                break;
            case WAIT_TO_DELIVERY:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_distribution);
                tv_blanket_order_status.setText(R.string.wait_delivery);
                break;
            case WAIT_SIGN_IN:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_signing);
                tv_blanket_order_status.setText(R.string.wait_sign_in);
                break;
            case DONE:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_ok);
                tv_blanket_order_status.setText(R.string.done);
                break;
        }
        tv_order_number.setText(mLogisticsBean.getAll_info().getOrder_sn());
        tv_receive_name.setText(mLogisticsBean.getAll_info().getBuyer_name());
        StringBuilder receivePhone = new StringBuilder();
        if (!TextUtils.isEmpty(mLogisticsBean.getAll_info().getBuyer_phone())) {
            receivePhone.append("+").append(mLogisticsBean.getAll_info().getBuyer_phone()).append("，");
        }
        if (!TextUtils.isEmpty(mLogisticsBean.getAll_info().getPhone())) {
            receivePhone.append("+").append(mLogisticsBean.getAll_info().getPhone()).append("，");
        }
        if (!TextUtils.isEmpty(mLogisticsBean.getAll_info().getMob())) {
            receivePhone.append("+").append(mLogisticsBean.getAll_info().getMob()).append("，");
        }
        tv_receive_phone.setText(TextUtils.isEmpty(receivePhone.toString()) ? "" : receivePhone.toString().substring(0, receivePhone.length() - 2));
        tv_receive_adddress.setText(mLogisticsBean.getAll_info().getRev());
        rv_content.setLayoutManager(new LinearLayoutManager(this));
        rv_content.setNestedScrollingEnabled(false);
        rv_content.setAdapter(new OrderDetailAdapter(this, mLogisticsBean, new OrderDetailAdapter.OnRefreshOrderDetail() {
            @Override
            public void onRefreshOrderDetail() {
                requestLogistics();
                EventBus.getDefault().post(QueryFragment.REFRESH_LOGISTICS_LIST);
            }
        }));
        if (TextUtils.isEmpty(mLogisticsBean.getAll_info().getType_name()) && blanketOrderState != DONE) {
            rl_bottom_button.setVisibility(View.GONE);
        } else {
            rl_bottom_button.setVisibility(View.VISIBLE);
            if(blanketOrderState == DONE){
                bt_handle.setText(R.string.look_delivery);
            }else {
                bt_handle.setText(mLogisticsBean.getAll_info().getType_name());
            }
//            if (mLogisticsBean.getAll_info().getStatus() == 2011 || mLogisticsBean.getAll_info().getStatus() == 2061) {
//                bt_handle.setEnabled(false);
//            } else {
//                bt_handle.setEnabled(true);
//            }
            bt_handle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (blanketOrderState == WAIT_SIGN_IN) {
                        Intent intent = new Intent(OrderDetailActivity.this, DeliveryActivity.class);
                        intent.putExtra(DeliveryActivity.KEY_TYPE, DeliveryActivity.TYPE_UPLOAD);
                        intent.putExtra(DeliveryActivity.KEY_ORDER_MAIN_ID, mLogisticsBean.getAll_info().getOrder_main_id());
                        startActivityForResult(intent, REQUEST_DELIVERY_CODE);
                        return;
                    }
                    if (blanketOrderState == DONE) {
                        Intent intent = new Intent(OrderDetailActivity.this, DeliveryActivity.class);
                        intent.putExtra(DeliveryActivity.KEY_TYPE, DeliveryActivity.TYPE_LOOK);
                        intent.putExtra(DeliveryActivity.KEY_ORDER_MAIN_ID, mLogisticsBean.getAll_info().getOrder_main_id());
                        intent.putExtra(DeliveryActivity.KEY_IMGS, mLogisticsBean.getAll_info().getImg());
                        startActivity(intent);
                        return;
                    }

                    DialogUtils.createTwoBtnDialog(OrderDetailActivity.this
                            , MessageFormat.format(getString(R.string.order_handle_confirm_tip)
                                    , mLogisticsBean.getAll_info().getType_name())
                            , getString(R.string.confirm)
                            , getString(R.string.cancel)
                            , new DialogUtils.OnRightBtnListener() {
                                @Override
                                public void setOnRightListener(Dialog dialog) {
                                    requestOrderLogisticsChange();
                                }
                            }, null, false, true);
                }
            });
        }
    }

    private void requestLogistics() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGISTICS_INFO())
                .addParam("sn_or_shipping", mLogisticsBean.getFid())
                .withPOST(new NetCallBack<LogisticsBean>(this) {
                    @Override
                    public void onSuccess(@NonNull LogisticsBean logisticsBean) {
                        if(isFinishing()){
                            return;
                        }
                        mLogisticsBean = logisticsBean;
                        showData();
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(OrderDetailActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @NotNull
                    @Override
                    public Class<LogisticsBean> getRealType() {
                        return LogisticsBean.class;
                    }
                }, false);
    }

    private void requestOrderLogisticsChange() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_LOGISTICS_CHANGE())
                .addParam("order_main_id", mLogisticsBean.getAll_info().getOrder_main_id())
                .withPOST(new NetCallBack<String>(this) {
                    @Override
                    public void onSuccess(@NonNull String str) {
                        EventBus.getDefault().post(QueryFragment.REFRESH_LOGISTICS_LIST);
                        Intent intent = new Intent(OrderDetailActivity.this, DoSuccesfulActivity.class);
                        startActivity(intent);
                        if(isFinishing()){
                            return;
                        }
                        finish();
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(OrderDetailActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }
                }, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_GOODS_CODE && resultCode == RESULT_OK) {
            requestLogistics();
        }
        if (requestCode == REQUEST_DELIVERY_CODE && resultCode == RESULT_OK) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if(KyyLogisticsApp.activityNum<=1){
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
        super.onBackPressed();
    }
}
