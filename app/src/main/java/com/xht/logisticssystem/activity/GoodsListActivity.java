package com.xht.logisticssystem.activity;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.logisticssystem.R;
import com.xht.logisticssystem.adapter.GoodsListAdapter;
import com.xht.logisticssystem.bean.GoodsListBean;
import com.xht.logisticssystem.fragment.QueryFragment;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

public class GoodsListActivity extends BaseActivity {

    private ImageView iv_back, iv_blanket_order_status;
    private TextView tv_title, tv_blanket_order_status, tv_store_name, tv_done_to_zhuhai, tv_tracking_number, tv_logistics_company, tv_contact_people,tv_contact_people_title;
    private RelativeLayout rl_logistics_info, rl_confirm_warehouse;
    private RecyclerView rv_content;
    private Button bt_confirm_warehouse;
    private View view_line;

    public static final String KEY_ORDER_ID = "key_order_id";

    private String mOrderId;

    @Override
    public int setLayoutResId() {
        return R.layout.activity_goods_list;
    }

    @Override
    public void initView() {
        iv_back = findViewById(R.id.iv_back);
        tv_title = findViewById(R.id.tv_title);
        iv_blanket_order_status = findViewById(R.id.iv_blanket_order_status);
        tv_blanket_order_status = findViewById(R.id.tv_blanket_order_status);
        tv_store_name = findViewById(R.id.tv_store_name);
        tv_done_to_zhuhai = findViewById(R.id.tv_done_to_zhuhai);
        tv_tracking_number = findViewById(R.id.tv_tracking_number);
        tv_logistics_company = findViewById(R.id.tv_logistics_company);
        tv_contact_people = findViewById(R.id.tv_contact_people);
        tv_contact_people_title = findViewById(R.id.tv_contact_people_title);
        rl_logistics_info = findViewById(R.id.rl_logistics_info);
        rl_confirm_warehouse = findViewById(R.id.rl_confirm_warehouse);
        rv_content = findViewById(R.id.rv_content);
        bt_confirm_warehouse = findViewById(R.id.bt_confirm_warehouse);
        view_line = findViewById(R.id.view_line);
        iv_back.setVisibility(View.VISIBLE);
        tv_title.setText(R.string.goods_list);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mOrderId = getIntent().getStringExtra(KEY_ORDER_ID);
        requestGoodsList(mOrderId);
    }

    private void requestGoodsList(String order_id) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_GOODS_LIST())
                .addParam("order_id", order_id)
                .withPOST(new NetCallBack<GoodsListBean>(this) {
                    @Override
                    public void onSuccess(@NonNull GoodsListBean goodsListBean) {
                        if(isFinishing()){
                            return;
                        }
                        if (goodsListBean.getInfo() != null) {
                            showData(goodsListBean);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @NotNull
                    @Override
                    public Class<GoodsListBean> getRealType() {
                        return GoodsListBean.class;
                    }
                }, false);
    }

    private void showData(@NonNull GoodsListBean goodsListBean) {
        int blanketOrderState = Utils.getBlanketOrderState(goodsListBean.getInfo().getLogistic_type());
        switch (blanketOrderState) {
            case OrderDetailActivity.WAIT_TO_ZHUHAI:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_warehouse);
                tv_blanket_order_status.setText(R.string.wait_to_zhuhai);
                break;
            case OrderDetailActivity.WAIT_CLEARANCE_OF_GOODS:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_customs);
                tv_blanket_order_status.setText(R.string.wait_clearance_of_goods);
                break;
            case OrderDetailActivity.WAIT_TO_MACAU:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_warehouse);
                tv_blanket_order_status.setText(R.string.wait_to_macau);
                break;
            case OrderDetailActivity.WAIT_TO_DELIVERY:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_distribution);
                tv_blanket_order_status.setText(R.string.wait_delivery);
                break;
            case OrderDetailActivity.WAIT_SIGN_IN:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_signing);
                tv_blanket_order_status.setText(R.string.wait_sign_in);
                break;
            case OrderDetailActivity.DONE:
                iv_blanket_order_status.setImageResource(R.mipmap.logistics_icon_ok);
                tv_blanket_order_status.setText(R.string.done);
                break;
        }
        tv_store_name.setText(goodsListBean.getInfo().getStore_name());
        if (goodsListBean.getInfo().getOrder_state() == 40
                && blanketOrderState==OrderDetailActivity.WAIT_TO_ZHUHAI) {
            tv_done_to_zhuhai.setVisibility(View.VISIBLE);
        } else {
            tv_done_to_zhuhai.setVisibility(View.GONE);
        }
        if (goodsListBean.getInfo().getOrder_state()==30
                || goodsListBean.getInfo().getOrder_state()==40) {
            rl_logistics_info.setVisibility(View.VISIBLE);
            view_line.setVisibility(View.VISIBLE);
            tv_tracking_number.setText(goodsListBean.getInfo().getShipping_code());
            tv_logistics_company.setText(goodsListBean.getInfo().getE_name());
            if(TextUtils.isEmpty(goodsListBean.getInfo().getTelphone())){
                tv_contact_people.setVisibility(View.GONE);
                tv_contact_people_title.setVisibility(View.GONE);
            }else {
                tv_contact_people.setVisibility(View.VISIBLE);
                tv_contact_people_title.setVisibility(View.VISIBLE);
                tv_contact_people.setText(goodsListBean.getInfo().getSeller_name() + "  " + goodsListBean.getInfo().getTelphone());
            }
        } else {
            rl_logistics_info.setVisibility(View.GONE);
            view_line.setVisibility(View.GONE);
        }
        rv_content.setLayoutManager(new LinearLayoutManager(this));
        rv_content.setNestedScrollingEnabled(false);
        rv_content.setAdapter(new GoodsListAdapter(this, goodsListBean));
        if (goodsListBean.getInfo().isPower1()&&goodsListBean.getInfo().getOrder_state()==30) {
            rl_confirm_warehouse.setVisibility(View.VISIBLE);
            bt_confirm_warehouse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bt_confirm_warehouse.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogUtils.createTwoBtnDialog(GoodsListActivity.this
                                    , getString(R.string.confirm_to_zhuhai_tip)
                                    , getString(R.string.confirm)
                                    , getString(R.string.cancel)
                                    , new DialogUtils.OnRightBtnListener() {
                                        @Override
                                        public void setOnRightListener(Dialog dialog) {
                                            requestOrderToZhuhai();
                                        }
                                    }, null, false, true);
                        }
                    });
                }
            });
        } else {
            rl_confirm_warehouse.setVisibility(View.GONE);
        }

    }

    private void requestOrderToZhuhai() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_ORDER_TO_ZHUHAI())
                .addParam("order_id", mOrderId)
                .withPOST(new NetCallBack<String>(this) {
                    @Override
                    public void onSuccess(@NonNull String str) {
                        EventBus.getDefault().post(QueryFragment.REFRESH_LOGISTICS_LIST);
                        if(isFinishing()){
                            return;
                        }
                        requestGoodsList(mOrderId);
                        setResult(RESULT_OK);
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(isFinishing()){
                            return;
                        }
                        Toast.makeText(GoodsListActivity.this, err, Toast.LENGTH_SHORT).show();
                    }

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }
                }, false);
    }

}
