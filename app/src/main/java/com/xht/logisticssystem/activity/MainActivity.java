package com.xht.logisticssystem.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.xht.logisticssystem.R;
import com.xht.logisticssystem.fragment.BaseFragment;
import com.xht.logisticssystem.fragment.MineFragment;
import com.xht.logisticssystem.fragment.QueryFragment;
import com.zxing_android.CaptureActivity;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    private RadioGroup rg_navigation;
    private View view_scan;
    private ArrayList<BaseFragment> mFragments;
    //当前显示的fragment
    private BaseFragment mCurrentFragment;

    private final String KEY_QUERY = "key_query";
    private final String KEY_MINE = "key_mine";
    private QueryFragment mQueryFragment;
    private MineFragment mMineFragment;
    private int mPosition;

    private final int PERMISSION_REQUEST_CODE = 20000;

    @Override
    public int setLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mQueryFragment = (QueryFragment) getSupportFragmentManager().getFragment(savedInstanceState, KEY_QUERY);
            mMineFragment = (MineFragment) getSupportFragmentManager().getFragment(savedInstanceState, KEY_MINE);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        initData();
        rg_navigation = findViewById(R.id.rg_navigation);
        view_scan = findViewById(R.id.view_scan);
        rg_navigation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_query:
                        mPosition = 0;
                        break;
                    case R.id.rb_mine:
                        mPosition = 1;
                        break;
                }
                BaseFragment baseFragment = getFragment(mPosition);
                switchFragment(mCurrentFragment, baseFragment);
            }
        });
        view_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MainActivity.this, com.zxing_android.CaptureActivity.class);
                    startActivity(intent);
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this
                            , new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
                }
            }
        });
        switchFragment(mCurrentFragment, getFragment(0));
    }

    private void initData() {
        mFragments = new ArrayList<>();
        if (mQueryFragment == null) {
            mQueryFragment = new QueryFragment();
        }
        if (mMineFragment == null) {
            mMineFragment = new MineFragment();
        }
        mFragments.add(mQueryFragment);
        mFragments.add(mMineFragment);
    }

    /**
     * 根据位置得到对应的 Fragment
     *
     * @param position 位置角标
     * @return 返回fragment
     */
    private BaseFragment getFragment(int position) {
        if (mFragments != null && mFragments.size() > 0) {
            return mFragments.get(position);
        }
        return null;
    }

    /**
     * 切换Fragment
     *
     * @param fragment     当前显示的fragment
     * @param nextFragment 即将要显示的fragment
     */
    private void switchFragment(Fragment fragment, BaseFragment nextFragment) {
        if (mCurrentFragment != nextFragment) {
            mCurrentFragment = nextFragment;
            if (nextFragment != null) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                //判断nextFragment是否添加成功
                if (!nextFragment.isAdded()) {
                    //隐藏当前的Fragment
                    if (fragment != null) {
                        transaction.hide(fragment);
                    }
                    //添加Fragment
                    transaction.add(R.id.fl_content, nextFragment).commit();
                } else {
                    //隐藏当前Fragment
                    if (fragment != null) {
                        transaction.hide(fragment);
                    }
                    transaction.show(nextFragment).commitAllowingStateLoss();
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mQueryFragment != null && mQueryFragment.isAdded()) {
            getSupportFragmentManager().putFragment(outState, KEY_QUERY, mQueryFragment);
        }
        if (mMineFragment != null && mMineFragment.isAdded()) {
            getSupportFragmentManager().putFragment(outState, KEY_MINE, mMineFragment);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(this, CaptureActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.open_camera_permission_tip, Toast.LENGTH_SHORT).show();
            }
        }
    }

}
