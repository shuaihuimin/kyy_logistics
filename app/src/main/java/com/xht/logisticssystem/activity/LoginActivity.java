package com.xht.logisticssystem.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xht.logisticssystem.R;
import com.xht.logisticssystem.bean.LoginBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Login;
import com.xht.logisticssystem.utils.Utils;

import org.jetbrains.annotations.NotNull;

public class LoginActivity extends BaseActivity {

    private Button bt_login;
    private EditText et_staff_number, et_password;
    public static final String KEY_NEED_LOGIN_TIP = "key_need_login_tip";//重新登录的提示

    @Override
    public int setLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    public void initView() {
        et_staff_number = findViewById(R.id.et_staff_number);
        et_password = findViewById(R.id.et_password);
        bt_login = findViewById(R.id.bt_login);
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = et_staff_number.getText().toString().trim();
                String password = et_password.getText().toString().trim();
                requestLogin(username, password);
            }
        });
        et_staff_number.addTextChangedListener(new MyTextWatcher());
        et_password.addTextChangedListener(new MyTextWatcher());
        boolean need_login = getIntent().getBooleanExtra(KEY_NEED_LOGIN_TIP,false);
        if(need_login){
            et_staff_number.postDelayed(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.createTipAllTextDialog(LoginActivity.this
                            ,getString(R.string.need_login_tip));
                }
            },1000);
        }
    }

    private void requestLogin(final String username, String password) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGIN())
                .addParam("type", "1")
                .addParam("username", username)
                .addParam("password", password)
                .addParam("client", Utils.isHUAWEI() ? "HUAWEI" : "android")
                .addParam("l_l_p_token", Login.Companion.getInstance().getHuawei_push_token())
                .withPOST(new NetCallBack<LoginBean>(this) {
                    @Override
                    public void onSuccess(@NonNull LoginBean loginBean) {
                        if (loginBean.getMember_info() != null) {
                            Login.Companion.getInstance().setToken(loginBean.getToken());
                            Login.Companion.getInstance().setToken_id(loginBean.getToken_id());
                            Login.Companion.getInstance().setLogistics_person_id(loginBean.getMember_info().getLogistics_person_id());
                            Login.Companion.getInstance().setLogistics_serial_number(
                                    loginBean.getMember_info().getLogistics_serial_number());
                            Login.Companion.getInstance().setLogistics_truename(
                                    loginBean.getMember_info().getLogistics_truename());
                            Login.Companion.getInstance().setUsername(username);
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        }

                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @NotNull
                    @Override
                    public Class<LoginBean> getRealType() {
                        return LoginBean.class;
                    }
                }, false);
    }

    class MyTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String username = et_staff_number.getText().toString();
            String password = et_password.getText().toString();
            if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
                bt_login.setEnabled(true);
            } else {
                bt_login.setEnabled(false);
            }
        }
    }

}
