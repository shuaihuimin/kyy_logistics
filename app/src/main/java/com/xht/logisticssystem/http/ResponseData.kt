package com.xht.logisticssystem.http


/**
 * 响应数据
 * @author hjgang
 * 基类
 */
class ResponseData{

    /** 状态码:200 | 304 | 404 | 500  */
    var flag: Int = 0
    /** 是否有下一页  */
    var err_code: Int = 0
    /** JSON格式的字符串  */
    var msg: String? = null
    /** 字符串结果  */
    var data: String? = null
    /** 总记录数  */



}
