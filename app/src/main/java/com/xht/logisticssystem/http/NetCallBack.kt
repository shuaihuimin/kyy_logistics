package com.xht.logisticssystem.http

import android.content.Context
import android.content.Intent
import android.support.annotation.NonNull
import com.xht.logisticssystem.activity.LoginActivity
import com.xht.logisticssystem.bean.LogisiticsListBean
import com.xht.logisticssystem.utils.Login
import com.xht.logisticssystem.utils.Utils


/**
 * Created by 俊华 on 2017/9/23.
 * 所有的okttp请求结果 基类
 */
abstract class NetCallBack<Result> constructor(context: Context) {

    private val mContext: Context

    init {
        mContext = context
    }

    /**
     * 成功
     */
    abstract fun onSuccess(@NonNull result: Result)

    /**
     * 失败  errCode 为自定义的ErrorStatus
     *
     */
    fun onSessionFailure(@NonNull errCode: Int, @NonNull err: String) {
        if (errCode == 11009) {
            //需要重新登录
            val intent = Intent()
            intent.setClass(mContext, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(LoginActivity.KEY_NEED_LOGIN_TIP,true)
            Login.getInstance().token="";
            Login.getInstance().token_id="";
            mContext.startActivity(intent)
            Utils.setIconBadgeNum(LogisiticsListBean.numBean())
        }else{

            onFailure(errCode,err);
        }
    }

    /**
     * 只能在kotlin语言中拿到，在java语言中处理过于复杂
     * 获取真实 Result 类型 如com.xht.kuaiyouyi.bean.CommentTags
     */
//    fun getRealType(): Class<Result> {
//        val gs = this@NetCallBack.javaClass.genericInterfaces[0] as ParameterizedType
//        return gs.actualTypeArguments[0] as Class<Result>
//    }
    abstract fun getRealType(): Class<Result>

    abstract fun onFailure(@NonNull errCode: Int, @NonNull err: String);


}