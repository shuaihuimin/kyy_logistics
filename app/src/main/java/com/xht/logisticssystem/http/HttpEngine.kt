package com.xht.logisticssystem.http


/**
 * Created by 俊华 on 2017/10/16.
 * okhttp   统一网络请求引擎规范
 */
interface HttpEngine {

    /**
     * get   请求方式
     */
    fun <T> get(url: String, parms: MutableMap<String, Any>, callBack: NetCallBack<T>?, isWorkThread:Boolean)


    /**
     * post  请求方式
     */
    fun <T> post(type:Int, url: String, parms: MutableMap<String, Any>, callBack: NetCallBack<T>?, isWorkThread:Boolean)
}