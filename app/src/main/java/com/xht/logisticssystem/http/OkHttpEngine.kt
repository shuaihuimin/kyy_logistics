package com.xht.logisticssystem.http

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import com.xht.logisticssystem.utils.JsonU.parseJsonWithFast
import com.xht.logisticssystem.utils.LogUtil
import com.xht.logisticssystem.utils.Login
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.lang.ref.WeakReference
import java.util.concurrent.Executors
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit


/**
 * Created by 俊华 on 2017/12/18 0018.
 */
@SuppressLint("NewApi")
class OkHttpEngine : HttpEngine {
    private val TAG = "OkHttpEngine"

    constructor() {
        LogUtil.e(TAG, "init  -------  OkHttpEngine")
    }

    inner class HttpLogger : HttpLoggingInterceptor.Logger {
        override fun log(message: String?) {

            LogUtil.e("Http-Logger", message)
        }

    }

    //json请求
    val JSON = MediaType
            .parse("application/json; charset=utf-8")


    private val client: OkHttpClient by lazy {
        val logInterceptor = HttpLoggingInterceptor(HttpLogger())
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addNetworkInterceptor(logInterceptor)
                .build()
    }


    private val mHandler: Handler by lazy {
        Handler(Looper.getMainLooper())
    }
    private val threadPool: ThreadPoolExecutor by lazy {
        Executors.newCachedThreadPool() as ThreadPoolExecutor
    }
    private var request: Request? = null
    override fun <T> get(url: String, params: MutableMap<String, Any>, callBack: NetCallBack<T>?, isWordThread: Boolean) {
        if (params.isEmpty()) {
            request = Request.Builder().url(url).build()
        } else {
            val urlStr = url + "?" + MapParamToString(params)
            request = Request.Builder().url(urlStr).build()
        }
        enqueue(request, callBack, isWordThread)
    }


    override fun <T> post(type: Int, url: String, parms: MutableMap<String, Any>, callBack: NetCallBack<T>?, isWordThread: Boolean) {
        LogUtil.e(TAG, "post url : " + url)

        if (parms.isEmpty()) {
            request = Request.Builder().url(url).build()
        } else {
            when (type) {
                0 -> {
                    val builder = FormBody.Builder()
                    //遍历map
                    for (entry in parms.entries) {
                        builder.add(entry.key, entry.value.toString())
                        LogUtil.e(TAG, "http body -->  " + entry.key + "   " + entry.value.toString())
                    }
                    request = Request.Builder().url(url).post(builder.build()).build()
                }
                1 -> { //文件
                    val builder = MultipartBody.Builder()
                    builder.setType(MultipartBody.FORM)
                    //遍历map
                    for (entry in parms.entries) {
                        if (entry.value is File) {
                            if (!(entry.value as File).exists()) {
                                LogUtil.e(TAG, "文件不存在 终止上传")
                                return
                            } else {
//                                application/octet-stream
//                                multipart/form-data
                                val fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), entry.value as File)
                                builder.addFormDataPart("upload_file[]", entry.key, fileBody)
                                LogUtil.e(TAG, "httpbody" + entry.key + "= value is File:" + (entry.value as File).absolutePath)
                            }
                        } else {
                            builder.addFormDataPart(entry.key, "" + entry.value)
                            LogUtil.e(TAG, "httpbody" + entry.key + "= value is string:" + entry.value)
                        }
                    }
                    request = Request.Builder().url(url).post(builder.build()).build()
                }
            }
        }
        enqueue(request, callBack, isWordThread)
    }

    /**
     *处理传入的请求参数
     */
    private fun MapParamToString(params: MutableMap<String, Any>?): String {
        if (params != null) {
            val stringBuilder = StringBuilder()
            val iterator = params.entries.iterator()
            while (iterator.hasNext()) {
                val entry = iterator.next()
                stringBuilder.append(entry.key + "=" + entry.value + "&")
            }
            return stringBuilder.toString().substring(0, stringBuilder.length - 1)
        }
        return ""
    }

    /**
     * 统一执行
     */
    @Synchronized
    private fun <T> enqueue(request: Request?, callBack: NetCallBack<T>?, isWordThread: Boolean) {
        LogUtil.e(TAG, "真实类型 = " + callBack?.getRealType()?.name)
        threadPool.execute {
            LogUtil.e(TAG, "-------------------  end  ------------------->>   " + Thread.currentThread().name)
            client.newCall(request)?.enqueue(object : Callback {
                override fun onFailure(call: Call?, e: IOException?) {
                    if (isWordThread) {
                        callBack?.onSessionFailure(ErrorStatus.NETWORK_ERROR, "网络异常")
                    } else {
                        mHandler.post {
                            callBack?.onSessionFailure(ErrorStatus.NETWORK_ERROR, "网络异常")
                        }
                    }
                }

                override fun onResponse(call: Call?, response: Response?) {
                    if (response != null) {
                        if (response.isSuccessful) {
                            when (callBack?.getRealType()?.name) {
//                                String::class.java.name -> dealString(callBack, isWordThread, response.body()?.string())
                            //将响应数据转化为输入流数据
                                File::class.java.name -> {
                                    dealFile(callBack, isWordThread, response.body()?.byteStream())
                                }
                                Bitmap::class.java.name -> dealBitmip(callBack, isWordThread, response.body()?.byteStream())
                                else -> dealClazz(callBack, isWordThread, response.body()?.string())
                            }
                        } else {//请求失败
                            if (isWordThread) {
                                callBack?.onSessionFailure(ErrorStatus.NETWORK_ERROR, "response.isFail")
                            } else {
                                mHandler.post {
                                    callBack?.onSessionFailure(ErrorStatus.NETWORK_ERROR, "response.isFail")
                                }
                            }
                        }
                    } else {    //response为空
                        if (isWordThread) {
                            callBack?.onSessionFailure(ErrorStatus.NETWORK_ERROR, "response == null")
                        } else {
                            mHandler.post {
                                callBack?.onSessionFailure(ErrorStatus.NETWORK_ERROR, "response == null")
                            }
                        }
                    }
                }
            })
        }

    }

    private fun <T> dealFile(callBack: NetCallBack<T>?, wordThread: Boolean, byteStream: InputStream?) {

        if (byteStream == null) {

            LogUtil.e(TAG, "文件下载失败")
            return
        }
        var len = 0
        var file: File = File(Environment.getExternalStorageDirectory(), "8884949.jpg")
        var fos = FileOutputStream(file)
        var buf = byteArrayOf()

        len = byteStream.read(buf)

        while (len != -1) {
            fos.write(buf, 0, len)
        }
        fos.flush()
        fos.close()
        byteStream.close()

    }


    /**
     * 处理图片
     */
    private fun <T> dealBitmip(callBack: NetCallBack<T>?, isWordThread: Boolean, inputStream: InputStream?) {
        //将输入流数据转化为Bitmap位图数据
        val weakBitmap: WeakReference<Bitmap> = WeakReference(BitmapFactory.decodeStream(inputStream))
        inputStream?.close()
        if (weakBitmap.get() != null) {
            if (isWordThread) {
                callBack?.onSuccess(weakBitmap.get() as T)
            } else {
                mHandler.post {
                    callBack?.onSuccess(weakBitmap.get() as T)
                }
            }
        } else {

            if (isWordThread) {
                callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "转换图片失败")
            } else {

                mHandler.post {
                    callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "转换图片失败")
                }


            }
        }
    }

    /**
     * 返回字符串
     */
    private fun <T> dealString(callBack: NetCallBack<T>?, isWordThread: Boolean, res: String?) {
        Log.i("info","---------------type-------------"+res);
        if (TextUtils.isEmpty(res)) {
            LogUtil.e(TAG, "dealString res = null")
            if (isWordThread) {
                callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "网络异常")
            } else {
                mHandler.post {
                    callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "网络异常")
                }
            }
        } else {
            LogUtil.e(TAG, "dealString = " + res)
            if (isWordThread) {
                callBack?.onSuccess(res as T)
            } else {
                mHandler.post {
                    callBack?.onSuccess(res as T)
                }
            }
        }

    }


    /**
     * 处理自定义类型
     */
    private fun <T> dealClazz(callBack: NetCallBack<T>?, isWordThread: Boolean, res: String?) {
        Log.i("info","---------------type-------------"+res);
        if (TextUtils.isEmpty(res)) {
            LogUtil.e(TAG, "dealClazz json为空  直接返回 ")
            if (isWordThread) {
                callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "解析错误")
            } else {
                mHandler.post {
                    callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "解析错误")
                }
            }
            return
        }
        LogUtil.e(TAG, "dealClazz = " + res)
        val responseData = parseJsonWithFast(res, ResponseData::class.java)
        if (responseData == null) {
            if (isWordThread) {
                callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "解析错误")
            } else {
                mHandler.post {
                    callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "解析错误")
                }
            }
        } else { //responseData != null

            when (responseData.err_code) {
                ErrorStatus.SUCCESS -> {//正确
                    if (TextUtils.isEmpty(responseData.data)) {
                        if (isWordThread) {
                            callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "responseData.data == null")
                        } else {
                            mHandler.post {
                                callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "responseData.data == null")
                            }
                        }
                    } else {
                        LogUtil.e(TAG, "data = " + responseData.data)
                        val any = parseJsonWithFast(responseData.data!!, callBack?.getRealType())
                        if (any == null) {
                            if (isWordThread) {
                                callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "解析错误")
                            } else {
                                mHandler.post {
                                    callBack?.onSessionFailure(ErrorStatus.PARSE_ERROR, "解析错误")
                                }
                            }
                        } else {
                            if (isWordThread) {
                                callBack?.onSuccess(any)
                            } else {
                                mHandler.post {
                                    callBack?.onSuccess(any)
                                }
                            }
                        }
                    }

                }
                ErrorStatus.TOKEN_ERROR -> {
                    Login.getInstance().token = ""
                    Login.getInstance().token_id = ""
//                    SharedPreferencesUtils.put(KyyApp.context,"login_token",null);
//                    SharedPreferencesUtils.put(KyyApp.context,"login_token_id",null);
                }
                ErrorStatus.SIGN_ERROR -> {
                    Login.getInstance().token = ""
                    Login.getInstance().token_id = ""
//                    SharedPreferencesUtils.put(KyyApp.context,"login_token",null);
//                    SharedPreferencesUtils.put(KyyApp.context,"login_token_id",null);
                }
                else -> {//错误码
                    if (isWordThread) {
                        callBack?.onSessionFailure(responseData.err_code, responseData.msg as String)
                    } else {
                        mHandler.post {
                            callBack?.onSessionFailure(responseData.err_code, responseData.msg as String)
                        }
                    }
                }

            }
        }


    }


}



