package com.xht.logisticssystem.http

import android.content.Context
import android.os.Build
import android.provider.Settings.Secure
import android.text.TextUtils
import com.xht.logisticssystem.KyyLogisticsApp
import com.xht.logisticssystem.utils.*
import java.util.regex.Pattern


/**
 * Created by 俊华 on 2017/10/16.
 *   网络请求工具类
 */

class NetUtil {

    private constructor() {
        LogUtil.e(TAG, "设备 uuid ---------  " + uuid)

        LogUtil.e(TAG, "设备 device_name ---------  " + device_name)

    }

    //上下文
    private lateinit var mContext: Context
    //请求类型 1是文件
    private var mType = 0
    //请求路径
    private var mUrl = "https://www.baidu.com"
    //请求参数
    private val mParams by lazy {
        mutableMapOf<String, Any>()
    }

    //网络引擎
    private val httpEngine by lazy {
        OkHttpEngine()
    }

    fun url(url: String?): NetUtil {
        if (TextUtils.isEmpty(url)) {
            throw Exception("请输入正确的URL")
        } else {
            val b = Pattern.compile(UL).matcher(url).matches()
            if (b) {
                mUrl = url as String
            } else {
                throw Exception("请输入正确的URL")
            }
        }
        return this
    }

    fun changeType(type: Int): NetUtil {
        mType = type
        return this
    }

    fun addParams(params: MutableMap<String, Any>): NetUtil {

        mParams.putAll(params)

        return this
    }

    fun addParam(key: String, value: Any): NetUtil {
        mParams.put(key, value)
        return this
    }

    /**
     * isWorkThread: Boolean = false  是否在子线程回调结果  String... s
     * "e","3"
     *
     */
    fun <T> withPOST(callBack: NetCallBack<T>?, isWorkThread: Boolean = false) {
        /**
         *   公共参数     参与排序
         */
        LogUtil.e(TAG, "murl : " + mUrl)
        addParam("device_uuid", uuid)
        addParam("device_name", device_name)
        /**
        当前时间
         */
        val t = SystemUtil.getTime(SystemUtil.getTimeStr("" + System.currentTimeMillis())) as String
        addParam("sign", SignUtil.sign(mParams, t))
        /**                           |
         *                           |
         *                          |
         * 公共参数      SignUtil.sign(mParams, t) 后不参与排序
         * LoginUtils.getToken_id()
         */
        addParam("token_id", Login.getInstance().token_id)
        addParam("sign_time", t)
        mParams.forEach {
            LogUtil.e(TAG, it.toString())
        }
        httpEngine.post(mType, mUrl, mParams, callBack, isWorkThread)
        mType = 0
        mParams.clear()
        LogUtil.e(TAG, "**************************************")
    }

    fun <T> withLoadPOST(callBack: NetCallBack<T>?, isWorkThread: Boolean = false, wl:String) {
//        /**
//         *   公共参数     参与排序
//         */
//        LogUtil.e(TAG, "murl : " + mUrl)
        addParam("device_uuid", uuid)
        addParam("device_name", device_name)
        if(!TextUtils.isEmpty(wl)){
            addParam("wl",wl)
            url(KyyConstants.URL_LODE)
        }
        /**
        当前时间
         */
        val t = SystemUtil.getTime(SystemUtil.getTimeStr("" + System.currentTimeMillis())) as String
        addParam("sign", SignUtil.sign(mParams, t))
        /**                           |
         *                           |
         *                          |
         * 公共参数      SignUtil.sign(mParams, t) 后不参与排序
         * LoginUtils.getToken_id()
         */
        addParam("token_id", Login.getInstance().token_id)
        addParam("sign_time", t)
//        mParams.forEach {
//            LogUtil.e(TAG, it.toString())
//        }
        httpEngine.post(mType, mUrl, mParams, callBack, isWorkThread)
        /**
         *   公共参数     参与排序
         */
        LogUtil.e(TAG, "murl : " + mUrl)
        mParams.forEach {
            LogUtil.e(TAG, it.toString())
        }
        mType = 0
        mParams.clear()
        LogUtil.e(TAG, "**************************************")
    }

    /**
     * 上传文件
     * 文件不能放进去签名加密
     */
    fun <T> withPOSTFile(callBack: NetCallBack<T>?, isWorkThread: Boolean = false) {
        LogUtil.e(TAG, "murl -> " + mUrl)
//        addParam("device_uuid", uuid)
//        addParam("device_name", device_name)
//        val t = SystemUtil.getTime(SystemUtil.getTimeStr("" + System.currentTimeMillis())) as String
//        addParam("sign", SignUtil.sign(mParams, t))
//        addParam("token_id", Login.getInstance().token_id)
//        addParam("sign_time", t)
        mParams.forEach {
            LogUtil.e(TAG, it.toString())
        }
        httpEngine.post(1, mUrl, mParams, callBack, isWorkThread)
        mType = 0
        mParams.clear()
        LogUtil.e(TAG, "**************************************")
    }


    fun <T> withGET(callBack: NetCallBack<T>?, isWorkThread: Boolean = false) {
        LogUtil.e(TAG, "mParams -> " + mParams.toString())
        httpEngine.get(mUrl, mParams, callBack, isWorkThread)
        mParams.clear()
    }

    companion object {
        const val TAG = "NetUtil"
        const val UL = "[a-zA-z]+://[^\\s]*"
        //        private val android_id = Secure.getString(KyyLogisticsApp.context.getContentResolver(), Secure.ANDROID_ID)
        var uuid = Secure.getString(KyyLogisticsApp.context.getContentResolver(), Secure.ANDROID_ID)

        var device_name = Build.MODEL
        fun getInstance(): NetUtil {
            return SingletonHolder.INSTANCE
        }
    }


    private object SingletonHolder {
        val INSTANCE = NetUtil()
    }

}