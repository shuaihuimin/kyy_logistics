package com.xht.logisticssystem.http

/**
 * Created by xuhao on 2017/12/5.
 * 请求结果状态码
 */
object ErrorStatus {
    /**
     * 响应成功
     */
    @JvmField
    val SUCCESS = 0

    /**
     * 未知错误
     */
    @JvmField
    val UNKNOWN_ERROR = 1002

    /**
     * 服务器内部错误
     */
    @JvmField
    val SERVER_ERROR = 1003

    /**
     * 网络连接超时
     */
    @JvmField
    val NETWORK_ERROR = 404

    /**
     * API解析异常（或者第三方数据结构更改）等其他异常
     */
    @JvmField
    val API_ERROR = 1005

    /**
     * 解析错误
     */
    @JvmField
    val PARSE_ERROR = 1003

    /**
     * token错误
     */
    @JvmField
    val TOKEN_ERROR = 11007

    /**
     * 签名错误
     */
    @JvmField
    val SIGN_ERROR = 11008

}