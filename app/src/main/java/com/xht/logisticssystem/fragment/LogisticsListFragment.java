package com.xht.logisticssystem.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.adapter.LogisticsListAdapter;
import com.xht.logisticssystem.bean.LogisiticsListBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.KyyConstants;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class LogisticsListFragment extends BaseFragment {

    private SmartRefreshLayout smartRefreshLayout;
    private ListView listView;
    private RelativeLayout rl_empty_logistics_list;
    private LogisticsListAdapter listAdapter;
    private List<LogisiticsListBean.ListBean> mlistBeans;
    public String wl ="";
    private int mType;
    public static final int TYPE_WAIT_TO_ZHUHAI = 0;//待入珠海仓
    public static final int TYPE_WAIT_CLEARANCE_OF_GOODS = 1;//待报关
    public static final int TYPE_WAIT_TO_MACAU = 2;//待入澳门仓
    public static final int TYPE_WAIT_DELIVERY = 3;//待配送
    public static final int TYPE_WAIT_SIGN_IN = 4;//待签收
    public static final int TYPE_DONE = 5;//已完成
    private OnToOrderDetailClickListener mListener;


    public static LogisticsListFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type",type);
        LogisticsListFragment fragment = new LogisticsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        wl = "";
        mType=getArguments().getInt("type");
        smartRefreshLayout= view.findViewById(R.id.smartfreshlayout);
        listView =  view.findViewById(R.id.lv_content);
        rl_empty_logistics_list =  view.findViewById(R.id.rl_empty_logistics_list);
        smartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(RefreshLayout refreshlayout) {
                getData();
            }
        });

        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                wl="";
                getData();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mListener != null){
                    mListener.onToOrderDetailClickListener(mlistBeans.get(position).getOrder_main_sn());
                }
            }
        });
        getData();
    }

    @Override
    protected int setLayoutResId() {
        return R.layout.fragment_logistics_list;
    }


    public void getData(){
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGISTICS_LIST())
                .addParam("type",mType)
                .withLoadPOST(new NetCallBack<LogisiticsListBean>(getActivity()) {

                    @NotNull
                    @Override
                    public Class<LogisiticsListBean> getRealType() {
                        return LogisiticsListBean.class;
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity() == null){
                            return;
                        }
                        Toast.makeText(getContext(),err,Toast.LENGTH_SHORT).show();
                        if(smartRefreshLayout.isRefreshing()){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.isLoading()){
                            smartRefreshLayout.finishLoadmore();
                        }
                    }

                    @Override
                    public void onSuccess(@NonNull LogisiticsListBean listBean) {
                        if(getActivity() == null){
                            return;
                        }
                        if(smartRefreshLayout.isRefreshing()){
                            smartRefreshLayout.finishRefresh();
                        }
                        if(smartRefreshLayout.isLoading()){
                            smartRefreshLayout.finishLoadmore();
                        }
                        if(TextUtils.isEmpty(wl)){
                            //刷新
                            smartRefreshLayout.resetNoMoreData();
                            if(listBean.getList()!=null && listBean.getList().size()>0){
                                rl_empty_logistics_list.setVisibility(View.GONE);
                                mlistBeans = listBean.getList();
                                listAdapter = new LogisticsListAdapter(getContext(),mlistBeans);
                                listView.setAdapter(listAdapter);
                                listView.setVisibility(View.VISIBLE);
                            }else {
                                //无数据
                                rl_empty_logistics_list.setVisibility(View.VISIBLE);
                                listView.setVisibility(View.GONE);
                            }
                        }else {
                            //加载更多
                            if(listBean.getList()!=null && listBean.getList().size()>0){
                                mlistBeans.addAll(listBean.getList());
                                listAdapter.notifyDataSetChanged();
                            }else if(listBean.getList()!=null&&listBean.getList().size()==0){
                                smartRefreshLayout.finishLoadmoreWithNoMoreData();
                                smartRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
                            }
                        }
                        EventBus.getDefault().post(listBean.getNum());
                        wl = listBean.getWl();
                    }
                },false, wl);
    }


    public interface OnToOrderDetailClickListener{
        /**
         * 运单号或者订单号
         * @param code
         */
        void onToOrderDetailClickListener(String code);
    }

    public void setListener(OnToOrderDetailClickListener listener){
        mListener = listener;
    }

}
