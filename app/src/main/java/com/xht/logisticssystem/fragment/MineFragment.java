package com.xht.logisticssystem.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.xht.logisticssystem.R;
import com.xht.logisticssystem.activity.LoginActivity;
import com.xht.logisticssystem.bean.LogisiticsListBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Login;
import com.xht.logisticssystem.utils.Utils;

import org.jetbrains.annotations.NotNull;

import de.hdodenhof.circleimageview.CircleImageView;

public class MineFragment extends BaseFragment {

    private CircleImageView civ_icon;
    private TextView tv_name, tv_staff_number, tv_phone;
    private Button bt_logout;

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        civ_icon = view.findViewById(R.id.civ_icon);
        tv_name = view.findViewById(R.id.tv_name);
        tv_staff_number = view.findViewById(R.id.tv_staff_number);
        tv_phone = view.findViewById(R.id.tv_phone);
        bt_logout = view.findViewById(R.id.bt_logout);
        bt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.createTwoBtnDialog(getActivity()
                        , getResources().getString(R.string.exit_tip)
                        , getResources().getString(R.string.confirm)
                        , getResources().getString(R.string.cancel)
                        , new DialogUtils.OnRightBtnListener() {
                            @Override
                            public void setOnRightListener(Dialog dialog) {
                                requestLogout();
                            }
                        }, null, true, true);

            }
        });
        tv_name.setText(Login.Companion.getInstance().getLogistics_truename());
        tv_staff_number.setText(Login.Companion.getInstance().getLogistics_serial_number());
        tv_phone.setText(Login.Companion.getInstance().getUsername());
    }

    private void requestLogout() {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGIN_OUT())
                .addParam("logistics_person_id", Login.Companion.getInstance().getLogistics_person_id())
                .withPOST(new NetCallBack<String>(getActivity()) {
                    @Override
                    public void onSuccess(@NonNull String str) {
                        Utils.setIconBadgeNum(new LogisiticsListBean.numBean());
                        Login.Companion.getInstance().setToken("");
                        Login.Companion.getInstance().setToken_id("");
                        Login.Companion.getInstance().setLogistics_person_id("");
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setClass(getActivity(), LoginActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                    }

                    @NotNull
                    @Override
                    public Class<String> getRealType() {
                        return String.class;
                    }
                }, false);

    }

    @Override
    protected int setLayoutResId() {
        return R.layout.fragment_mine;
    }
}
