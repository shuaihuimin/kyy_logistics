package com.xht.logisticssystem.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.flyco.tablayout.SlidingTabLayout;
import com.xht.logisticssystem.R;
import com.xht.logisticssystem.activity.EmptyInfoActivity;
import com.xht.logisticssystem.activity.OrderDetailActivity;
import com.xht.logisticssystem.bean.LogisiticsListBean;
import com.xht.logisticssystem.bean.LogisticsBean;
import com.xht.logisticssystem.http.NetCallBack;
import com.xht.logisticssystem.http.NetUtil;
import com.xht.logisticssystem.utils.DialogUtils;
import com.xht.logisticssystem.utils.KyyConstants;
import com.xht.logisticssystem.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class QueryFragment extends BaseFragment implements LogisticsListFragment.OnToOrderDetailClickListener{
    private EditText et_search;
    private SlidingTabLayout tb_tab;
    private ViewPager vp_content;

    private final int[] TAB_TITLES = new int[]{R.string.wait_to_zhuhai
            , R.string.wait_clearance_of_goods, R.string.wait_to_macau
            , R.string.wait_delivery, R.string.wait_sign_in, R.string.done};
    private List<LogisticsListFragment> mFragments;
    private MyAdapter adapter;

    public static final String REFRESH_LOGISTICS_LIST = "LogisticsList";


    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        et_search = view.findViewById(R.id.et_search);
        tb_tab = view.findViewById(R.id.tb_tab);
        vp_content = view.findViewById(R.id.vp_content);

        mFragments = new ArrayList<>();
        LogisticsListFragment zhuhaiFragment = LogisticsListFragment.newInstance(LogisticsListFragment.TYPE_WAIT_TO_ZHUHAI);
        zhuhaiFragment.setListener(this);
        mFragments.add(zhuhaiFragment);
        LogisticsListFragment clearanceFragment =LogisticsListFragment.newInstance(LogisticsListFragment.TYPE_WAIT_CLEARANCE_OF_GOODS);
        clearanceFragment.setListener(this);
        mFragments.add(clearanceFragment);
        LogisticsListFragment macauFragment = LogisticsListFragment.newInstance(LogisticsListFragment.TYPE_WAIT_TO_MACAU);
        macauFragment.setListener(this);
        mFragments.add(macauFragment);
        LogisticsListFragment deliveryFragment = LogisticsListFragment.newInstance(LogisticsListFragment.TYPE_WAIT_DELIVERY);
        deliveryFragment.setListener(this);
        mFragments.add(deliveryFragment);
        LogisticsListFragment signinFragment = LogisticsListFragment.newInstance(LogisticsListFragment.TYPE_WAIT_SIGN_IN);
        signinFragment.setListener(this);
        mFragments.add(signinFragment);
        LogisticsListFragment doneFragment =LogisticsListFragment.newInstance(LogisticsListFragment.TYPE_DONE);
        doneFragment.setListener(this);
        mFragments.add(doneFragment);
        adapter = new MyAdapter(getChildFragmentManager());
        vp_content.setAdapter(adapter);
        tb_tab.setViewPager(vp_content);
        vp_content.setCurrentItem(LogisticsListFragment.TYPE_WAIT_TO_ZHUHAI);


        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String code = et_search.getText().toString().trim();
                    if (!TextUtils.isEmpty(code)) {
                        requestLogistics(code);
                    }
                    return true;
                }
                return false;
            }
        });
        EventBus.getDefault().register(this);
    }

    @Override
    protected int setLayoutResId() {
        return R.layout.fragment_query;
    }

    private void requestLogistics(String code) {
        NetUtil.Companion.getInstance().url(KyyConstants.INSTANCE.getURL_LOGISTICS_INFO())
                .addParam("sn_or_shipping", code)
                .withPOST(new NetCallBack<LogisticsBean>(getActivity()) {
                    @Override
                    public void onSuccess(@NonNull LogisticsBean logisticsBean) {
                        if(getActivity() == null){
                            return;
                        }
                        Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
                        intent.putExtra(OrderDetailActivity.KEY_ORDERDETAIL, logisticsBean);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(@NonNull int errCode, @NonNull @NotNull String err) {
                        if(getActivity() == null){
                            return;
                        }
                        if (errCode == 30000) {
                            //运单号或订单号不存在
                            DialogUtils.createTipAllTextDialog(getContext(), err);
                        } else if (errCode == 30001) {
                            //无此订单物流
                            Intent intent = new Intent(getActivity(), EmptyInfoActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getContext(), err, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @NotNull
                    @Override
                    public Class<LogisticsBean> getRealType() {
                        return LogisticsBean.class;
                    }
                }, false);
    }

    @Override
    public void onToOrderDetailClickListener(String code) {
        requestLogistics(code);
    }


    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public LogisticsListFragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        //重写这个方法，将设置每个Tab的标题
        @Override
        public CharSequence getPageTitle(int position) {
            return getString(TAB_TITLES[position]);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(String eventType){
        if(eventType.equals(REFRESH_LOGISTICS_LIST)){
            for (LogisticsListFragment fragment:mFragments){
                if(fragment.isAdded()){
                    fragment.wl="";
                    fragment.getData();
                }
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LogisiticsListBean.numBean _numBean){
        if(_numBean.getWait_first() > 0){
            tb_tab.showMsg(LogisticsListFragment.TYPE_WAIT_TO_ZHUHAI, _numBean.getWait_first());
            tb_tab.setMsgMargin(LogisticsListFragment.TYPE_WAIT_TO_ZHUHAI, 0, 10);
            if (tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_TO_ZHUHAI) != null) {
                tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_TO_ZHUHAI).setBackgroundColor(Color.RED);
            }
        }else {
            tb_tab.hideMsg(LogisticsListFragment.TYPE_WAIT_TO_ZHUHAI);
        }

        if(_numBean.getWait_second() > 0){
            tb_tab.showMsg(LogisticsListFragment.TYPE_WAIT_CLEARANCE_OF_GOODS, _numBean.getWait_second());
            tb_tab.setMsgMargin(LogisticsListFragment.TYPE_WAIT_CLEARANCE_OF_GOODS, 0, 10);
            if (tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_CLEARANCE_OF_GOODS) != null) {
                tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_CLEARANCE_OF_GOODS).setBackgroundColor(Color.RED);
            }
        }else {
            tb_tab.hideMsg(LogisticsListFragment.TYPE_WAIT_CLEARANCE_OF_GOODS);
        }

        if(_numBean.getWait_third() > 0){
            tb_tab.showMsg(LogisticsListFragment.TYPE_WAIT_TO_MACAU, _numBean.getWait_third());
            tb_tab.setMsgMargin(LogisticsListFragment.TYPE_WAIT_TO_MACAU, 0, 10);
            if (tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_TO_MACAU) != null) {
                tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_TO_MACAU).setBackgroundColor(Color.RED);
            }
        }else {
            tb_tab.hideMsg(LogisticsListFragment.TYPE_WAIT_TO_MACAU);
        }

        if(_numBean.getWait_forth() > 0){
            tb_tab.showMsg(LogisticsListFragment.TYPE_WAIT_DELIVERY, _numBean.getWait_forth());
            tb_tab.setMsgMargin(LogisticsListFragment.TYPE_WAIT_DELIVERY, 0, 10);
            if (tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_DELIVERY) != null) {
                tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_DELIVERY).setBackgroundColor(Color.RED);
            }
        }else {
            tb_tab.hideMsg(LogisticsListFragment.TYPE_WAIT_DELIVERY);
        }

        if(_numBean.getWait_fifth() > 0){
            tb_tab.showMsg(LogisticsListFragment.TYPE_WAIT_SIGN_IN, _numBean.getWait_fifth());
            tb_tab.setMsgMargin(LogisticsListFragment.TYPE_WAIT_SIGN_IN, 0, 10);
            if (tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_SIGN_IN) != null) {
                tb_tab.getMsgView(LogisticsListFragment.TYPE_WAIT_SIGN_IN).setBackgroundColor(Color.RED);
            }
        }else {
            tb_tab.hideMsg(LogisticsListFragment.TYPE_WAIT_SIGN_IN);
        }

        Utils.setIconBadgeNum(_numBean);
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
}
