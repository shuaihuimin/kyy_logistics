package com.xht.logisticssystem.bean;

import java.util.List;

public class LogisiticsListBean {

    /**
     * list : [{"order_main_id":75,"order_main_sn":"1000000000019801","addressee":"花花","tel":"853-13456789422","mob":"853-11111-11","address":"上海 上海市 盧灣區-的方法带上飞机迪斯科飞机"},{"order_main_id":184,"order_main_sn":"1000000000030701","addressee":"猪猪侠","tel":"86-15678902356","address":"澳門 澳門特別行政區-大梅沙"},{"order_main_id":188,"order_main_sn":"1000000000031101","addressee":"花花","tel":"853-13456789422","mob":"853-11111-11","address":"上海 上海市 盧灣區-的方法带上飞机迪斯科飞机"},{"order_main_id":191,"order_main_sn":"1000000000031401","addressee":"霍伊特","tel":"86-13417073722","address":"北京 北京市 朝陽區-32"},{"order_main_id":199,"order_main_sn":"1000000000032201","addressee":"花花","tel":"853-13456789422","mob":"853-11111-11","address":"上海 上海市 盧灣區-的方法带上飞机迪斯科飞机"},{"order_main_id":215,"order_main_sn":"1000000000033801","addressee":"花花","tel":"853-13456789422","mob":"853-11111-11","address":"上海 上海市 盧灣區-的方法带上飞机迪斯科飞机"},{"order_main_id":216,"order_main_sn":"1000000000033901","addressee":"苏展鸿","tel":"86-13160679580","address":"天津 天津市 和平區-4564"},{"order_main_id":241,"order_main_sn":"1000000000036401","addressee":"猪猪侠","tel":"86-15678902356","address":"澳門 澳門特別行政區-大梅沙"},{"order_main_id":244,"order_main_sn":"1000000000036701","addressee":"花花","tel":"853-13456789422","mob":"853-11111-11","address":"上海 上海市 盧灣區-的方法带上飞机迪斯科飞机"},{"order_main_id":252,"order_main_sn":"1000000000037501","addressee":"花花","tel":"853-13456789422","mob":"853-11111-11","address":"上海 上海市 盧灣區-的方法带上飞机迪斯科飞机"}]
     * kyy : eyJQIjoyLCJLIjoid2xfMDAxIiwiTyI6MX0=
     */

    private String wl;
    private List<ListBean> list;
    private numBean num = new numBean();

    public String getWl() {
        return wl;
    }

    public void setWl(String wl) {
        this.wl = wl;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public numBean getNum() {
        return num;
    }

    public void setNum(numBean logistics_wait_num) {
        this.num = logistics_wait_num;
    }

    public static class ListBean {
        /**
         * order_main_id : 75
         * order_main_sn : 1000000000019801
         * addressee : 花花
         * tel : 853-13456789422
         * mob : 853-11111-11
         * address : 上海 上海市 盧灣區-的方法带上飞机迪斯科飞机
         */

        private int order_main_id;
        private String order_main_sn;
        private String addressee;
        private String tel;
        private String phone;
        private String mob;
        private String address;

        public int getOrder_main_id() {
            return order_main_id;
        }

        public void setOrder_main_id(int order_main_id) {
            this.order_main_id = order_main_id;
        }

        public String getOrder_main_sn() {
            return order_main_sn;
        }

        public void setOrder_main_sn(String order_main_sn) {
            this.order_main_sn = order_main_sn;
        }

        public String getAddressee() {
            return addressee;
        }

        public void setAddressee(String addressee) {
            this.addressee = addressee;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getMob() {
            return mob;
        }

        public void setMob(String mob) {
            this.mob = mob;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }

    public static class numBean {

        /**
         * wait_first : 49
         * wait_second : 55
         * wait_third : 1
         * wait_forth : 2
         * wait_fifth : 13
         */

        private int wait_first;
        private int wait_second;
        private int wait_third;
        private int wait_forth;
        private int wait_fifth;

        public int getWait_first() {
            return wait_first;
        }

        public void setWait_first(int wait_first) {
            this.wait_first = wait_first;
        }

        public int getWait_second() {
            return wait_second;
        }

        public void setWait_second(int wait_second) {
            this.wait_second = wait_second;
        }

        public int getWait_third() {
            return wait_third;
        }

        public void setWait_third(int wait_third) {
            this.wait_third = wait_third;
        }

        public int getWait_forth() {
            return wait_forth;
        }

        public void setWait_forth(int wait_forth) {
            this.wait_forth = wait_forth;
        }

        public int getWait_fifth() {
            return wait_fifth;
        }

        public void setWait_fifth(int wait_fifth) {
            this.wait_fifth = wait_fifth;
        }
    }
}
