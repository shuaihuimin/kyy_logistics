package com.xht.logisticssystem.bean;

public class LoginBean {


    /**
     * token : 57814fb08643d0eadd31e968c96181c2
     * member_info : {"logistics_serial_number":"001","logistics_username":"13823078888","is_delete":0,"logistics_truename":"李伟","logistics_person_id":1,"logistics_power":"0,1,2,3,4,5"}
     */

    private String token;
    private String token_id;
    private MemberInfoBean member_info;

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public MemberInfoBean getMember_info() {
        return member_info;
    }

    public void setMember_info(MemberInfoBean member_info) {
        this.member_info = member_info;
    }

    public static class MemberInfoBean {
        /**
         * logistics_serial_number : 001
         * logistics_username : 13823078888
         * is_delete : 0
         * logistics_truename : 李伟
         * logistics_person_id : 1
         * logistics_power : 0,1,2,3,4,5
         */

        private String logistics_serial_number;
        private String logistics_username;
        private int is_delete;
        private String logistics_truename;
        private String logistics_person_id;
        private String logistics_power;

        public String getLogistics_serial_number() {
            return logistics_serial_number;
        }

        public void setLogistics_serial_number(String logistics_serial_number) {
            this.logistics_serial_number = logistics_serial_number;
        }

        public String getLogistics_username() {
            return logistics_username;
        }

        public void setLogistics_username(String logistics_username) {
            this.logistics_username = logistics_username;
        }

        public int getIs_delete() {
            return is_delete;
        }

        public void setIs_delete(int is_delete) {
            this.is_delete = is_delete;
        }

        public String getLogistics_truename() {
            return logistics_truename;
        }

        public void setLogistics_truename(String logistics_truename) {
            this.logistics_truename = logistics_truename;
        }

        public String getLogistics_person_id() {
            return logistics_person_id;
        }

        public void setLogistics_person_id(String logistics_person_id) {
            this.logistics_person_id = logistics_person_id;
        }

        public String getLogistics_power() {
            return logistics_power;
        }

        public void setLogistics_power(String logistics_power) {
            this.logistics_power = logistics_power;
        }
    }
}
