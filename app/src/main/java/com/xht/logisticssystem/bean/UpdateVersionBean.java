package com.xht.logisticssystem.bean;

public class UpdateVersionBean {


    /**
     * res : {"version":"V1.0.0","forceUpdate":false,"is_update":false,"upgradePoint":"更新缘由test","installPackage":""}
     */

    private ResBean res;

    public ResBean getRes() {
        return res;
    }

    public void setRes(ResBean res) {
        this.res = res;
    }

    public static class ResBean {
        /**
         * version : V1.0.0
         * forceUpdate : false
         * is_update : false
         * upgradePoint : 更新缘由test
         * installPackage :
         */

        private String version;
        private boolean forceUpdate;
        private boolean is_update;
        private String upgradePoint;
        private String installPackage;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public boolean isForceUpdate() {
            return forceUpdate;
        }

        public void setForceUpdate(boolean forceUpdate) {
            this.forceUpdate = forceUpdate;
        }

        public boolean isIs_update() {
            return is_update;
        }

        public void setIs_update(boolean is_update) {
            this.is_update = is_update;
        }

        public String getUpgradePoint() {
            return upgradePoint;
        }

        public void setUpgradePoint(String upgradePoint) {
            this.upgradePoint = upgradePoint;
        }

        public String getInstallPackage() {
            return installPackage;
        }

        public void setInstallPackage(String installPackage) {
            this.installPackage = installPackage;
        }
    }
}
