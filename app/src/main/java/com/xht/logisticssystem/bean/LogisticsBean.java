package com.xht.logisticssystem.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LogisticsBean implements Serializable{


    /**
     * fid : sfeeee121
     * type_title : 待入珠海仓
     * type_pic : home-icon
     * all_info : {"order_main_id":1210,"buyer_name":"runmin","buyer_phone":"13456789422","order_sn":"1000000000136901","type":2,"status":2011,"rev":"吉林 吉林市 永吉县 的身份当时","type_name":""}
     * order_info : [{"order_id":1305,"store_id":1,"store_name":"快优易自营","shipping_code":"sfeeee121","order_state":40,"e_name":"顺丰快递","seller_name":"张生","telphone":"13800138000"}]
     * power1 : true
     */

    private String fid;
    private String type_title;
    private String type_pic;
    private AllInfoBean all_info;
    private boolean power1;
    private List<OrderInfoBean> order_info;

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getType_title() {
        return type_title;
    }

    public void setType_title(String type_title) {
        this.type_title = type_title;
    }

    public String getType_pic() {
        return type_pic;
    }

    public void setType_pic(String type_pic) {
        this.type_pic = type_pic;
    }

    public AllInfoBean getAll_info() {
        return all_info;
    }

    public void setAll_info(AllInfoBean all_info) {
        this.all_info = all_info;
    }

    public boolean isPower1() {
        return power1;
    }

    public void setPower1(boolean power1) {
        this.power1 = power1;
    }

    public List<OrderInfoBean> getOrder_info() {
        return order_info;
    }

    public void setOrder_info(List<OrderInfoBean> order_info) {
        this.order_info = order_info;
    }

    public static class AllInfoBean implements Serializable{
        /**
         * order_main_id : 1210
         * buyer_name : runmin
         * buyer_phone : 13456789422
         * order_sn : 1000000000136901
         * type : 2
         * status : 2011
         * rev : 吉林 吉林市 永吉县 的身份当时
         * type_name :
         */

        private int order_main_id;
        private String buyer_name;
        private String buyer_phone;
        private String order_sn;
        private int type;
        private int status;
        private String rev;
        private String type_name;
        private String phone;
        private String mob;
        private ArrayList<String> img;

        public int getOrder_main_id() {
            return order_main_id;
        }

        public void setOrder_main_id(int order_main_id) {
            this.order_main_id = order_main_id;
        }

        public String getBuyer_name() {
            return buyer_name;
        }

        public void setBuyer_name(String buyer_name) {
            this.buyer_name = buyer_name;
        }

        public String getBuyer_phone() {
            return buyer_phone;
        }

        public void setBuyer_phone(String buyer_phone) {
            this.buyer_phone = buyer_phone;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getRev() {
            return rev;
        }

        public void setRev(String rev) {
            this.rev = rev;
        }

        public String getType_name() {
            return type_name;
        }

        public void setType_name(String type_name) {
            this.type_name = type_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getMob() {
            return mob;
        }

        public void setMob(String mob) {
            this.mob = mob;
        }

        public ArrayList<String> getImg() {
            return img;
        }

        public void setImg(ArrayList<String> img) {
            this.img = img;
        }
    }

    public static class OrderInfoBean implements Serializable{
        /**
         * order_id : 1305
         * store_id : 1
         * store_name : 快优易自营
         * shipping_code : sfeeee121
         * order_state : 40
         * e_name : 顺丰快递
         * seller_name : 张生
         * telphone : 13800138000
         */

        private String order_id;
        private int store_id;
        private String store_name;
        private String shipping_code;
        private int order_state;
        private String e_name;
        private String seller_name;
        private String telphone;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public int getStore_id() {
            return store_id;
        }

        public void setStore_id(int store_id) {
            this.store_id = store_id;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getShipping_code() {
            return shipping_code;
        }

        public void setShipping_code(String shipping_code) {
            this.shipping_code = shipping_code;
        }

        public int getOrder_state() {
            return order_state;
        }

        public void setOrder_state(int order_state) {
            this.order_state = order_state;
        }

        public String getE_name() {
            return e_name;
        }

        public void setE_name(String e_name) {
            this.e_name = e_name;
        }

        public String getSeller_name() {
            return seller_name;
        }

        public void setSeller_name(String seller_name) {
            this.seller_name = seller_name;
        }

        public String getTelphone() {
            return telphone;
        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }
    }
}
