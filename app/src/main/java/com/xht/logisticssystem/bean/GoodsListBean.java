package com.xht.logisticssystem.bean;

import java.util.List;

public class GoodsListBean {


    /**
     * info : {"type_title":"待入珠海仓","logistic_type":0,"store_name":"快优易自营","order_sn":"1000000000134502","buyer_name":"helloworld","buyer_email":"shaobin000@foxmail.com","buyer_phone":"13169692393","goods_amount":"100.00","shipping_fee":"5.00","order_amount":"105.00","order_state":30,"shipping_code":"sf20181091715","e_name":"顺丰快递","seller_name":"张生","telphone":"13800138000","power1":true}
     * goods : [{"order_id":1281,"goods_name":"自行走曲臂式GTBZ-AE","goods_price":"100.00","goods_num":1,"goods_image":"http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179.png","goods_pay_price":"100.00","goods_spec":null}]
     */

    private InfoBean info;
    private List<GoodsBean> goods;

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public List<GoodsBean> getGoods() {
        return goods;
    }

    public void setGoods(List<GoodsBean> goods) {
        this.goods = goods;
    }

    public static class InfoBean {
        /**
         * logistic_type : 0
         * store_name : 快优易自营
         * order_sn : 1000000000134502
         * buyer_name : helloworld
         * buyer_email : shaobin000@foxmail.com
         * buyer_phone : 13169692393
         * goods_amount : 100.00
         * shipping_fee : 5.00
         * order_amount : 105.00
         * order_state : 30
         * shipping_code : sf20181091715
         * e_name : 顺丰快递
         * seller_name : 张生
         * telphone : 13800138000
         * power1 : true
         */

        private int logistic_type;
        private String store_name;
        private String order_sn;
        private String buyer_name;
        private String buyer_email;
        private String buyer_phone;
        private String goods_amount;
        private String shipping_fee;
        private String order_amount;
        private int order_state;
        private String shipping_code;
        private String e_name;
        private String seller_name;
        private String telphone;
        private boolean power1;

        public int getLogistic_type() {
            return logistic_type;
        }

        public void setLogistic_type(int logistic_type) {
            this.logistic_type = logistic_type;
        }

        public String getStore_name() {
            return store_name;
        }

        public void setStore_name(String store_name) {
            this.store_name = store_name;
        }

        public String getOrder_sn() {
            return order_sn;
        }

        public void setOrder_sn(String order_sn) {
            this.order_sn = order_sn;
        }

        public String getBuyer_name() {
            return buyer_name;
        }

        public void setBuyer_name(String buyer_name) {
            this.buyer_name = buyer_name;
        }

        public String getBuyer_email() {
            return buyer_email;
        }

        public void setBuyer_email(String buyer_email) {
            this.buyer_email = buyer_email;
        }

        public String getBuyer_phone() {
            return buyer_phone;
        }

        public void setBuyer_phone(String buyer_phone) {
            this.buyer_phone = buyer_phone;
        }

        public String getGoods_amount() {
            return goods_amount;
        }

        public void setGoods_amount(String goods_amount) {
            this.goods_amount = goods_amount;
        }

        public String getShipping_fee() {
            return shipping_fee;
        }

        public void setShipping_fee(String shipping_fee) {
            this.shipping_fee = shipping_fee;
        }

        public String getOrder_amount() {
            return order_amount;
        }

        public void setOrder_amount(String order_amount) {
            this.order_amount = order_amount;
        }

        public int getOrder_state() {
            return order_state;
        }

        public void setOrder_state(int order_state) {
            this.order_state = order_state;
        }

        public String getShipping_code() {
            return shipping_code;
        }

        public void setShipping_code(String shipping_code) {
            this.shipping_code = shipping_code;
        }

        public String getE_name() {
            return e_name;
        }

        public void setE_name(String e_name) {
            this.e_name = e_name;
        }

        public String getSeller_name() {
            return seller_name;
        }

        public void setSeller_name(String seller_name) {
            this.seller_name = seller_name;
        }

        public String getTelphone() {
            return telphone;
        }

        public void setTelphone(String telphone) {
            this.telphone = telphone;
        }

        public boolean isPower1() {
            return power1;
        }

        public void setPower1(boolean power1) {
            this.power1 = power1;
        }
    }

    public static class GoodsBean {
        /**
         * order_id : 1281
         * goods_name : 自行走曲臂式GTBZ-AE
         * goods_price : 100.00
         * goods_num : 1
         * goods_image : http://192.168.0.2/data/upload/shop/store/goods/1/1_05796293756348179.png
         * goods_pay_price : 100.00
         * goods_spec : null
         */

        private int order_id;
        private String goods_name;
        private String goods_price;
        private int goods_num;
        private String goods_image;
        private String goods_pay_price;
        private String goods_spec;
        private int is_second_hand;

        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }

        public String getGoods_name() {
            return goods_name;
        }

        public void setGoods_name(String goods_name) {
            this.goods_name = goods_name;
        }

        public String getGoods_price() {
            return goods_price;
        }

        public void setGoods_price(String goods_price) {
            this.goods_price = goods_price;
        }

        public int getGoods_num() {
            return goods_num;
        }

        public void setGoods_num(int goods_num) {
            this.goods_num = goods_num;
        }

        public String getGoods_image() {
            return goods_image;
        }

        public void setGoods_image(String goods_image) {
            this.goods_image = goods_image;
        }

        public String getGoods_pay_price() {
            return goods_pay_price;
        }

        public void setGoods_pay_price(String goods_pay_price) {
            this.goods_pay_price = goods_pay_price;
        }

        public String getGoods_spec() {
            return goods_spec;
        }

        public void setGoods_spec(String goods_spec) {
            this.goods_spec = goods_spec;
        }

        public int getIs_second_hand() {
            return is_second_hand;
        }

        public void setIs_second_hand(int is_second_hand) {
            this.is_second_hand = is_second_hand;
        }
    }
}
